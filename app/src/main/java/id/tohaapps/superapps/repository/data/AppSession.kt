package id.tohaapps.superapps.repository.data

import android.content.Context
import android.content.Context.MODE_PRIVATE
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import id.tohaapps.superapps.support.stringToObject
import id.tohaapps.superapps.view.login.model.User
import id.tohaapps.superapps.view.login.model.UserModel
import id.tohaapps.superapps.view.login.model.UserNoId

class AppSession(var context: Context) {
    private var session = context.getSharedPreferences("enesmart-session", MODE_PRIVATE)
    private var editor = session.edit()

    fun saveDataProfile(data: String) {
        editor.putString("user", data).commit()
    }

    val profile: UserModel?
        get() {
            val value = session.getString("user", null)
            return value?.let { stringToObject(it, UserModel::class.java) }
        }

    fun createSession(data: User) {
        editor.apply {
            data.id?.let { putInt("id", it) }
            putString("name", data.name)
            putString("mobile", data.mobile)
            putString("email", data.email)
        }.commit()
    }

    fun updateSession(data: User) {
        editor.apply {
            data.id?.let { putInt("id", it) }
            putString("name", data.name)
            putString("mobile", data.mobile)
            putString("email", data.email)
        }.commit()
    }

    fun updateSession(data: UserNoId) {
        editor.apply {
            putString("name", data.name)
            putString("mobile", data.mobile)
            putString("email", data.email)
        }.commit()
    }

//    fun getUser(): User {
//        return User(
//            session.getInt("id", -1),
//            session.getString("name", null),
//            session.getString("mobile", null),
//            session.getString("email", null)
//        )
//    }

//    fun getToken(): String? = profile?.token

    fun isLogin(isLogin: Boolean) {
        editor.putBoolean("isLogin", isLogin)
        editor.commit()
    }

    fun isLogin(): Boolean = session.getBoolean("isLogin", false)

    fun clearSession() {
        editor.clear()
        editor.commit()

        logoutFacebook()
        logoutGoogle()
    }

    private fun logoutGoogle() {
        val googleSignInOption = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .requestProfile()
            .build()

        val mGoogleSignClient = GoogleSignIn.getClient(
            context,
            googleSignInOption
        )

        mGoogleSignClient.signOut()
    }

    private fun logoutFacebook() {
        LoginManager.getInstance()
    }

}
package id.tohaapps.superapps.repository.api

import okhttp3.FormBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface MainApi {

    @POST("auth/signup")
    fun signUp(@Body body: FormBody): Call<String>

    @POST("auth/signin")
    fun signIn(@Body body: FormBody): Call<String>

    @POST("auth/signin/social")
    fun signInSosmed(@Body body: FormBody): Call<String>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("user/me")
    fun getProfile(@Header("Authorization") auth: String): Call<String>

    @PUT("user/mobile")
    fun updateProfile(
        @Header("Authorization") auth: String,
        @Body body: FormBody
    ): Call<String>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("banner/list")
    fun getBanner(@Header("Authorization") auth: String): Call<String>

    @POST("points")
    fun addPoint(
        @Header("Authorization") auth: String,
        @Body body: FormBody
    ): Call<String>

    @POST("points/share")
    fun addPointShare(
        @Header("Authorization") auth: String,
        @Body body: FormBody
    ): Call<String>

    @POST("points/watch")
    fun addPointWatch(
        @Header("Authorization") auth: String,
        @Body body: FormBody
    ): Call<String>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("points")
    fun getPoint(@Header("Authorization") auth: String): Call<String>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("iklan/list")
    fun getIklan(@Header("Authorization") auth: String): Call<String>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("shop/list")
    fun getShop(@Header("Authorization") auth: String): Call<String>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("product/promo")
    fun getProductPromo(@Header("Authorization") auth: String): Call<String>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("product/laris")
    fun getProductLaris(@Header("Authorization") auth: String): Call<String>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("event/list")
    fun getEvent(@Header("Authorization") auth: String): Call<String>

    @Headers("Accept: application/json")
    @POST("status")
    fun createStatus(
        @Header("Authorization") auth: String,
        @Body body: RequestBody
    ): Call<String>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("status/list")
    fun getAllStatus(@Header("Authorization") auth: String): Call<String>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("status/me")
    fun getMyStatus(@Header("Authorization") auth: String): Call<String>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("points/ranking")
    fun getRanking(@Header("Authorization") auth: String): Call<String>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("points/rank")
    fun getListRanking(
        @Header("Authorization") auth: String,
        @Query("page") page: Int
    ): Call<String>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("voucher/list")
    fun getAllVoucher(@Header("Authorization") auth: String): Call<String>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("voucher/mine")
    fun getMyVoucher(@Header("Authorization") auth: String): Call<String>

    @Headers("Accept: application/json")
    @POST("voucher/{id}/redeem")
    fun redeemVoucher(
        @Header("Authorization") auth: String,
        @Path("id") id: String
    ): Call<String>

}
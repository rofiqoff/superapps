package id.tohaapps.superapps.support.exoplayersupport

interface CallBacks {
    abstract fun callbackObserver(obj: Any)

    interface playerChangeListener {
        fun onPlayVideo()
        fun onIdleVideo()
        fun onBufferVideo()
        fun onEndVideo()
    }

    interface playerCallBack {
        fun onItemClickOnItem(albumId: Int?)

        fun onPlayingEnd()
    }
}
package id.tohaapps.superapps.support

const val ERROR_MESSAGE = "Mohon maaf, server kami sedang mengalami gangguan. Coba beberapa saat lagi :("
const val FAILED_MESSAGE = "Koneksi jaringan tidak stabil. Periksa kembali sambungan internet Anda"
const val FAILED_LOGOUT = "Session Anda telah habis"

const val NOT_FOUND_MESSAGE = "Nomer telepon belum terdaftar"
const val ALREADY_EXIST_MESSAGE = "Nomer telepon sudah terdaftar, gunakan nomer telepon yg lain"
const val MUST_NUMBER_MESSAGE = "Masukan nomer telepon dengan benar"
const val NEED_MORE_POINT = "Mohon maaf point Anda tidak cukup"

const val WEEK_EVENT = "Minggu ini"
const val MONTH_EVENT = "Bulan ini"
const val NEXT_MONTH_EVENT = "Bulan depan"
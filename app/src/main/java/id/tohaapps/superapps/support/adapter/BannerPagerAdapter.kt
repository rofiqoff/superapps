package id.tohaapps.superapps.support.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.viewpager.widget.PagerAdapter
import id.tohaapps.superapps.R
import id.tohaapps.superapps.support.baseUrl
import id.tohaapps.superapps.support.displayImage
import id.tohaapps.superapps.view.home.item.home.model.IklanModel

class BannerPagerAdapter(mContext: Context?, attachments: List<IklanModel>?) : PagerAdapter() {

    private var mContext: Context? = null
    private var attachments: List<IklanModel>? = null
    private var scaleType: ImageView.ScaleType = ImageView.ScaleType.FIT_XY

    private var mOnItemClickListener: OnItemClickListener? = null

    init {
        this.mContext = mContext
        this.attachments = attachments
    }

    override fun getCount(): Int {
        return attachments?.size as Int
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView =
            LayoutInflater.from(mContext).inflate(R.layout.content_row_slider, container, false)
        showImageHistory(itemView, position)

        container.addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }

    private fun showImageHistory(itemView: View, position: Int) {
        val imageView: AppCompatImageView? = itemView.findViewById(R.id.image)

        val image = attachments?.get(position)?.img

        val imageUrl = if (image?.startsWith("/") == true) {
            val a = image.removeRange(0, 0)
            "$baseUrl$a"
        } else {
            image
        }

        imageView?.scaleType = scaleType

        imageView?.displayImage(mContext, imageUrl)

        itemView.setOnClickListener { v ->
            if (mOnItemClickListener != null) {
                mOnItemClickListener?.onItemClick(v, position)
            }
        }
    }

    fun setScaleType(scaleType: ImageView.ScaleType) {
        this.scaleType = scaleType
    }

    fun setOnItemClickListener(mItemClickListener: OnItemClickListener) {
        this.mOnItemClickListener = mItemClickListener
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }

}
package id.tohaapps.superapps.support.customview

import android.content.Context
import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.Paint
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import id.tohaapps.superapps.R

class CountDrawable(context: Context) : Drawable() {

    private val mBadgePaint: Paint = Paint()
    private val mBadgePaint1: Paint = Paint()
    private var mWillDraw: Boolean = false

    init {
        mBadgePaint.color = ContextCompat.getColor(context.applicationContext, R.color.colorRed)
        mBadgePaint.isAntiAlias = true
        mBadgePaint.style = Paint.Style.FILL

        mBadgePaint1.color = ContextCompat.getColor(context.applicationContext, R.color.colorWhite)
        mBadgePaint1.isAntiAlias = true
        mBadgePaint1.style = Paint.Style.FILL
    }

    override fun draw(canvas: Canvas) {

        if (!mWillDraw) {
            return
        }
        val bounds = bounds
        val width = (bounds.right - bounds.left).toFloat()
        val height = (bounds.bottom - bounds.top).toFloat()

        // Position the badge in the top-right quadrant of the icon.

        /*Using Math.max rather than Math.min */

        val radius = Math.max(width, height) / 2 / 2
        val centerX = width - radius - 1f + 5
        val centerY = radius - 5

        // Draw badge circle.
        canvas.drawCircle(centerX, centerY, (radius + 5.5).toInt().toFloat(), mBadgePaint1)
        canvas.drawCircle(centerX, centerY, (radius + 1.5).toInt().toFloat(), mBadgePaint)
    }

    /*
    Sets the count (i.e notifications) to display.
     */
    fun showBadge(isShow: Boolean) {
        // Only draw a badge if there are notifications.
        mWillDraw = isShow
        invalidateSelf()
    }

    override fun setAlpha(alpha: Int) {
        // do nothing
    }

    override fun setColorFilter(cf: ColorFilter?) {
        // do nothing
    }

    override fun getOpacity(): Int {
        return PixelFormat.UNKNOWN
    }
}

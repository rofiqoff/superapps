package id.tohaapps.superapps.support.customview

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.view.Gravity
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import id.tohaapps.superapps.R
import java.io.File

class ImageChooser(internal var context: Context, internal var activity: Activity) {

    companion object {
        val PERMISSION_CODE_CAMERA = 123
        val PERMIISION_CODE_WRITE1 = 456
        val PERMIISION_CODE_WRITE2 = 789
        val RESULT_CODE_CAMERA = 111
        val RESULT_CODE_GALLERY = 222
    }

    fun ImageChooserDialog(title: String) {
        val dialogBuilder = AlertDialog.Builder(activity)
        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.dialog_chose_image, null, false)

        dialogBuilder.setView(view)

        val alertDialog = dialogBuilder.create()
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.window?.setGravity(Gravity.BOTTOM)

        alertDialog?.show()

        alertDialog.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )

        val mTitle = view.findViewById<TextView>(R.id.title)
        val mCamera = view.findViewById<Button>(R.id.camera)
        val mGalery = view.findViewById<Button>(R.id.gallery)

        mTitle.text = title
        mCamera.setOnClickListener {
            if (Build.VERSION.SDK_INT >= 23) {
                permissionWriteExternalCamera()
            } else {
                Camera()
            }
            alertDialog.dismiss()
        }

        mGalery.setOnClickListener {
            if (Build.VERSION.SDK_INT >= 23) {
                permissionWriteExternalGalery()
            } else {
                Gallery()
            }
            alertDialog.dismiss()
        }
    }

    private fun permissionWriteExternalCamera() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                PERMIISION_CODE_WRITE1
            )
        } else {
            permissionCamera()
        }
    }

    private fun permissionWriteExternalGalery() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                PERMIISION_CODE_WRITE2
            )
        } else {
            Gallery()
        }
    }

    private fun permissionCamera() {
        if (ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity, arrayOf(Manifest.permission.CAMERA),
                PERMISSION_CODE_CAMERA
            )
        } else {
            this.Camera()
        }
    }

    fun Camera() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (takePictureIntent.resolveActivity(activity.packageManager) != null) {
                activity.startActivityForResult(
                    takePictureIntent,
                    RESULT_CODE_CAMERA
                )
            }
        } else {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            activity.startActivityForResult(
                takePictureIntent,
                RESULT_CODE_CAMERA
            )
        }
    }

    fun Gallery() {
        val path = File(Environment.getExternalStorageDirectory().toString() + "/enesis")
        if (!path.exists()) {
            path.mkdirs()
        }

        val galleryIntent =
            Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        galleryIntent.type = "image/*"
        activity.startActivityForResult(
            galleryIntent,
            RESULT_CODE_GALLERY
        )
    }
}
package id.tohaapps.superapps.view.home.item.event.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EventModel(
    var id: String?,
    var judul: String?,
    var deskripsi: String?,
    var peserta: String?,
    var waktu: String?,
    var penyelenggara: String?,
    var lokasi: String?,
    var latlng: String?,
    var img: String?,
    var url: String?
) : Parcelable
package id.tohaapps.superapps.view.home.item.shop.model

data class StoreModel(var id: String?, var nama: String?, var url: String?)

data class ProductModel(var id: String?, var nama: String?, var img: String?)
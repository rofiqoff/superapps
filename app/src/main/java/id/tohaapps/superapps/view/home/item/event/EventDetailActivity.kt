package id.tohaapps.superapps.view.home.item.event

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.tohaapps.superapps.R
import id.tohaapps.superapps.support.*
import id.tohaapps.superapps.view.home.item.event.model.EventModel
import kotlinx.android.synthetic.main.activity_event_detail.*

class EventDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_detail)
        initView()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    private fun initView() {
        initToolbar()

        val data = intent?.getParcelableExtra<EventModel>("data")

        image_content?.displayImage(baseContext, data?.img)

        text_time?.text = dateFormat(data?.waktu)
        text_group?.text = data?.peserta
        text_title?.text = data?.judul
        text_description?.text = data?.deskripsi
        text_location?.text = data?.lokasi

        val url = data?.url

        showLog("LogUrl : $url")

        if (url.isNullOrEmpty()) {
            button_check?.gone()
        }

        button_check?.setOnClickListener { openWebPage(url) }

    }

    private fun initToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

}

package id.tohaapps.superapps.view.ranking.viewholder

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import id.tohaapps.superapps.R
import id.tohaapps.superapps.support.formatPoint
import id.tohaapps.superapps.support.gone
import id.tohaapps.superapps.view.ranking.model.ItemRangkingModel
import kotlinx.android.synthetic.main.item_list_ranking_top_3.view.*

class RankingTopViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

    fun onBind(data: ItemRangkingModel?, myRanking: String?, position: Int) {
        val no = data?.no.toString()
        val name = data?.name
        val point = data?.poin

        when (no) {
            "1" -> itemView.image?.setImageDrawable(
                ContextCompat.getDrawable(
                    itemView.context,
                    R.drawable.ic_rank_1
                )
            )
            "2" -> itemView.image?.setImageDrawable(
                ContextCompat.getDrawable(
                    itemView.context,
                    R.drawable.ic_rank_2
                )
            )
            "3" -> itemView.image?.setImageDrawable(
                ContextCompat.getDrawable(
                    itemView.context,
                    R.drawable.ic_rank_3
                )
            )
        }

        itemView.text_name?.text = name
        itemView.text_point?.text = formatPoint(point)

        if (myRanking == no) {
            itemView.root?.setCardBackgroundColor(
                ContextCompat.getColor(
                    itemView.context,
                    R.color.colorBlueSoftBackground
                )
            )
        }

        if (position > 0) {
            itemView.space?.gone()
        }
    }
}
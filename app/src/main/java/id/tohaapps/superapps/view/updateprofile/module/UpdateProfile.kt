package id.tohaapps.superapps.view.updateprofile.module

import okhttp3.FormBody

interface IUpdateProfileIteractor{
    fun updateProfile(auth: String, body: FormBody): Pair<Int, String?>
}

interface IUpdateProfilePresenter{
    fun updateProfile(name: String, mobile: String)
}
package id.tohaapps.superapps.view.login.module

import android.content.Context
import id.tohaapps.superapps.repository.AppApiClient
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.repository.data.AppSession
import id.tohaapps.superapps.support.showLog
import id.tohaapps.superapps.support.stringToObject
import id.tohaapps.superapps.view.login.model.User
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.FormBody
import org.json.JSONObject

class LoginPresenter(var context: Context, var view: ViewNetworkState) : ILoginPresenter {

    private val iteractor by lazy { LoginIteractor(AppApiClient.mainClient()) }
    private val session by lazy { AppSession(context) }

    val loginParam = "login.param"
    val loginSosmedParam = "login.sosmed.param"

    override fun loginSosmed(name: String, email: String) {
        view.networkState = NetworkingState.ShowLoading(Pair(loginSosmedParam, true))
        GlobalScope.launch {
            val formBody = FormBody.Builder()
                .add("name", name)
                .add("email", email)
                .build()

            val response = iteractor.loginSosmed(formBody)
            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(loginSosmedParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        val body = JSONObject(response.second.toString())

                        val userString = body.getString("user")
                        val user = stringToObject(userString, User::class.java)
                        val token = body.getString("access_token")

                        session.clearSession()
                        session.saveDataProfile(response.second.toString())

                        showLog("TOKEN SOSMED Bearer: $token")

                        session.createSession(user)

                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(loginSosmedParam, "success"))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            loginSosmedParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }

    override fun login(mobile: String) {
        view.networkState = NetworkingState.ShowLoading(Pair(loginParam, true))
        GlobalScope.launch {

            val formBody = FormBody.Builder().add("mobile", mobile).build()

            val response = iteractor.login(formBody)
            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(loginParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        val body = JSONObject(response.second.toString())

                        val userString = body.getString("user")
                        val user = stringToObject(userString, User::class.java)
                        val token = body.getString("access_token")

                        session.clearSession()
                        session.saveDataProfile(response.second.toString())

                        showLog("TOKEN LOGIN Bearer: $token")

                        session.createSession(user)

                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(loginSosmedParam, "success"))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            loginParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }

}
package id.tohaapps.superapps.view.home.item.event.listener

interface ScheduleEventListner {
    fun onSelectSchedule(position: Int, value: String)
}
package id.tohaapps.superapps.view.home.item.home

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.share.Sharer
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.tohaapps.superapps.R
import id.tohaapps.superapps.modulecommon.point.PointPresenter
import id.tohaapps.superapps.modulecommon.point.model.PointModel
import id.tohaapps.superapps.repository.IView
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.base.BaseFragment
import id.tohaapps.superapps.support.*
import id.tohaapps.superapps.support.adapter.Adapter
import id.tohaapps.superapps.support.adapter.BannerPagerAdapter
import id.tohaapps.superapps.support.customview.CustomDividerItemDecoration
import id.tohaapps.superapps.view.coupon.CouponActivity
import id.tohaapps.superapps.view.home.item.home.listener.HomeListener
import id.tohaapps.superapps.view.home.item.home.model.IklanModel
import id.tohaapps.superapps.view.home.item.home.module.BannerPresenter
import id.tohaapps.superapps.view.home.item.home.module.IklanPresenter
import id.tohaapps.superapps.view.home.item.home.viewholder.PromoViewHolder
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.row_content_shimmer_promo.*

class HomeFragment : BaseFragment(), ViewPager.OnPageChangeListener,
    ViewNetworkState, IView,
    HomeListener {

    private val presenterIklan by lazy { context?.let { IklanPresenter(it, this) } }
    private val presenterBanner by lazy { context?.let { BannerPresenter(it, this) } }
    private val presenterPoint by lazy { context?.let { PointPresenter(it, this) } }

    private var bannerAdapter: BannerPagerAdapter? = null
    private val callbackManager by lazy { CallbackManager.Factory.create() }
    private val shareDialog by lazy { ShareDialog(this) }

    enum class Request {
        none, point, banner, iklan
    }

    private var requestType = Request.none

    private var dots: Array<ImageView?>? = null
    private var handler: Handler? = null
    private var point = 0
    private var dotsCount: Int = 0
    private var page = 0
    private val delay = 5000L
    private var iklanId = ""
    private var memo = ""
    private var isShared = false

    private var runnable: Runnable = object : Runnable {
        override fun run() {
            if (bannerAdapter != null) {
                if (bannerAdapter?.count == page)
                    page = 0
                else
                    page++
                view_pager?.setCurrentItem(page, true)
                handler?.postDelayed(this, delay)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this item
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handler = Handler()
        handler?.postDelayed(runnable, delay)
        initView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        page = position

        for (i in 0 until dotsCount) {
            dots?.get(i)?.setImageDrawable(context?.let {
                ContextCompat.getDrawable(
                    it,
                    R.drawable.indicator_view_nonselected
                )
            })
        }

        dots?.get(position)?.setImageDrawable(context?.let {
            ContextCompat.getDrawable(
                it,
                R.drawable.indicator_view_selected
            )
        })
    }

    override fun onPause() {
        super.onPause()
        handler?.removeCallbacks(runnable)
    }

    override fun onResume() {
        super.onResume()
        requestPoint()
    }

    override fun onDestroy() {
        super.onDestroy()
        networkState = NetworkingState.Destroy()
        shimmer_layout?.gone()
        shimmer_layout?.stopShimmerAnimation()
    }

    override fun onShareItem(isShared: Boolean?, iklanId: String?, memo: String, url: String?) {
        this.iklanId = iklanId.toString()
        this.memo = memo
        this.isShared = isShared ?: false

        if (ShareDialog.canShow(ShareLinkContent::class.java)) {
            val content = ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(url))
                .build()

            shareDialog.show(content)
        }
    }

    override fun onAddPoint(iklanId: String, memo: String) {
        requestAddPointWatch(iklanId, memo)
    }

    override fun onItemListClick(data: IklanModel) {
        BottomSheetPlayVideo.newInstance(data, this).show(childFragmentManager, "play video")
    }

    override fun showLoading(key: String, status: Boolean) {
        super.showLoading(key, status)
        activity?.runOnUiThread {
            when (key) {
                presenterPoint?.getPointParam -> {
                    loading_point?.apply { if (status) visible() else gone() }
                    text_point?.apply { if (status) invisible() else visible() }
                }
                presenterPoint?.addPointWatchParam -> {
                    loading_point?.apply { if (status) visible() else gone() }
                    text_point?.apply { if (status) invisible() else visible() }
                }
                presenterPoint?.addPointShareParam -> {
                    loading_point?.apply { if (status) visible() else gone() }
                    text_point?.apply { if (status) invisible() else visible() }
                }
                presenterPoint?.addPointParam -> {
                    loading_point?.apply { if (status) visible() else gone() }
                    text_point?.apply { if (status) invisible() else visible() }
                }
                presenterBanner?.getBannerParam -> {
                    loading_banner?.apply { if (status) visible() else gone() }
                }
                presenterIklan?.getIklanParam -> {
//                    swipe_refresh?.isRefreshing = status

                    shimmer_layout?.apply {
                        if (status) {
                            visible()
                            startShimmerAnimation()
                        } else {
                            gone()
                            stopShimmerAnimation()
                        }
                    }

                    parent_list?.apply { if (status) gone() else visible() }
                }
            }
        }
    }

    override fun requestSuccess(key: String, response: Any?) {
        super.requestSuccess(key, response)
        activity?.runOnUiThread {
            when (key) {
                presenterPoint?.getPointParam -> {
                    val respon = response.toString()

                    val model = stringToObject(respon, PointModel::class.java)

                    point = model.data.poin.toString().toInt()

                    text_point?.text = formatPoint(point)

                    if (requestType == Request.point) {
                        requestIklan(true)
                    }

                }
                presenterPoint?.addPointWatchParam -> requestPoint(true)
                presenterPoint?.addPointShareParam -> {
                    val dataPoint = response.toString()

                    val message = "Selamat! Poin Anda bertambah $dataPoint poin"
                    activity?.showSnackBar(parent, message, true)

                    requestPoint(true)
                }
                presenterPoint?.addPointParam -> requestPoint(true)
                presenterBanner?.getBannerParam -> {
                    val STATUS_TYPE_Object = object : TypeToken<List<IklanModel>>() {}.type
                    val listData = Gson().fromJson<List<IklanModel>>(
                        response.toString(), STATUS_TYPE_Object
                    )
                    setBanner(listData as ArrayList<IklanModel>)
                }
                presenterIklan?.getIklanParam -> {
                    val STATUS_TYPE_Object = object : TypeToken<List<IklanModel>>() {}.type
                    val listData = Gson().fromJson<List<IklanModel>>(
                        response.toString(), STATUS_TYPE_Object
                    )
                    initList(listData as ArrayList<IklanModel>)
                }
            }
        }
    }

    override fun requestFailure(key: String, code: Int, message: Any?) {
        super.requestFailure(key, code, message)
        activity?.runOnUiThread {
            if (message.toString().isNotEmpty()) {
                activity?.showToast(message.toString())
            }

            when (key) {
                presenterPoint?.getPointParam -> {
                }
                presenterPoint?.addPointWatchParam -> {
                }
                presenterPoint?.addPointShareParam -> {
                }
                presenterPoint?.addPointParam -> {
                }
                presenterBanner?.getBannerParam -> {
                }
                presenterIklan?.getIklanParam -> {
                }
            }
        }
    }

    override fun initView() {
        requestBanner()
        requestIklan()
        requestPoint()

        to_change_point?.setOnClickListener {
            activity?.showActivity(
                CouponActivity::class.java,
                CouponActivity.EXTRA_TYPE_COUPON,
                CouponActivity.TYPE_CHANGE_POINT,
                CouponActivity.EXTRA_POINT,
                point.toString()
            )
        }

        to_my_coupon?.setOnClickListener {
            activity?.showActivity(
                CouponActivity::class.java,
                CouponActivity.EXTRA_TYPE_COUPON,
                CouponActivity.TYPE_MY_COUPON,
                CouponActivity.EXTRA_POINT,
                point.toString()
            )
        }

        swipe_refresh?.apply {
            setColorSchemeResources(R.color.colorAccent, R.color.colorTextOrange, R.color.colorRed)
            setOnRefreshListener {
                hide()
                requestIklan(true)
            }
        }

        shareDialog.registerCallback(callbackManager, object : FacebookCallback<Sharer.Result> {
            override fun onSuccess(result: Sharer.Result?) {
                if (isShared) {
                    val message =
                        "Terimakasih, bagikan kembali besok untuk mendapatkan point"
                    activity?.showSnackBar(parent, message, true)
                } else {
                    requestAddPointShare(iklanId, memo)
                }
            }

            override fun onCancel() {
                showLog("Cancel From Facebook")
            }

            override fun onError(error: FacebookException?) {
                showLog("Error From Facebook : ${error?.message}")
                activity?.showToast("Mohon maaf, terjadi kesalahan. Silahkan coba lagi", true)
            }

        })
    }

    private fun initList(data: ArrayList<IklanModel>) {
        val adapterPromo = object : Adapter<IklanModel, PromoViewHolder>(
            R.layout.row_content_promo,
            PromoViewHolder::class.java, IklanModel::class.java, data
        ) {
            override fun bindView(holder: PromoViewHolder, model: IklanModel, position: Int) {
                holder.onBind(activity, model, this@HomeFragment)
            }
        }

        list_promo?.adapter = adapterPromo
        list_promo?.layoutManager = LinearLayoutManager(context)
        list_promo?.isNestedScrollingEnabled = false

        list_promo?.addItemDecoration(
            CustomDividerItemDecoration(context)
        )

    }

    private fun setBanner(listBanner: ArrayList<IklanModel>) {
        pager_indicator?.removeAllViews()

        bannerAdapter = BannerPagerAdapter(context, listBanner)
        bannerAdapter?.setScaleType(ImageView.ScaleType.FIT_XY)

        view_pager?.adapter = bannerAdapter
        view_pager?.currentItem = 0
        view_pager?.addOnPageChangeListener(this)

        bannerAdapter?.setOnItemClickListener(object :
            BannerPagerAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {

            }
        })

        setUiPageViewController()
    }

    private fun requestBanner(isRefresh: Boolean = false) {
        requestType = Request.none
        if (isRefresh) requestType = Request.banner
        presenterBanner?.getBanner()
    }

    private fun requestPoint(isRefresh: Boolean = false) {
        requestType = Request.none
        if (isRefresh) requestType = Request.point
        presenterPoint?.getPoint()
    }

    private fun requestAddPointWatch(iklanId: String, memo: String) {
        requestType = Request.point
        presenterPoint?.addPointWatch(iklanId, memo)
    }

    private fun requestAddPointShare(iklanId: String, memo: String) {
        requestType = Request.point
        presenterPoint?.addPointShare(iklanId, memo)

    }

    private fun requestIklan(isRefresh: Boolean = false) {
        requestType = Request.none
        if (isRefresh) requestType = Request.iklan
        presenterIklan?.getIklan()
    }

    private fun loadingStatus(status: Boolean) {
        when (requestType) {
            Request.banner -> {
                loading_banner?.apply { if (status) visible() else gone() }
            }
            Request.point -> {
                loading_point?.apply { if (status) visible() else gone() }
                text_point?.apply { if (status) invisible() else visible() }
            }
            Request.iklan -> swipe_refresh?.isRefreshing = status
            Request.none -> {
                if (status) {
                    swipe_refresh?.disable()
                    loading_point?.visible()
                    text_point?.invisible()

                    shimmer_layout?.visible()
                    shimmer_layout?.startShimmerAnimation()

                    parent_list?.gone()

                    loading_banner?.visible()

                } else {
                    swipe_refresh?.enable()
                    loading_point?.gone()
                    text_point?.visible()

                    shimmer_layout?.gone()
                    shimmer_layout?.stopShimmerAnimation()
                    parent_list?.visible()

                    loading_banner?.gone()
                }
            }
        }
    }

    private fun setUiPageViewController() {
        dotsCount = bannerAdapter?.count.toString().toInt()
        dots = arrayOfNulls(dotsCount)

        try {
            for (i in 0 until dotsCount) {
                dots!![i] = ImageView(context)

                dots?.get(i)?.setImageDrawable(
                    context?.let {
                        ContextCompat.getDrawable(
                            it,
                            R.drawable.indicator_view_nonselected
                        )
                    }
                )

                val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )

                params.setMargins(10, 0, 10, 0)

                pager_indicator?.addView(dots?.get(i), params)
            }

            if (dots?.isEmpty() == true) {
                return
            } else {
                dots?.get(0)?.setImageDrawable(
                    context?.let {
                        ContextCompat.getDrawable(
                            it, R.drawable.indicator_view_selected
                        )
                    }
                )
            }

        } catch (e: Exception) {
            showLog("TagErorUiPageControler : ${e.message}")
        }
    }

}

package id.tohaapps.superapps.view.ranking.module

interface IRankingIteractor {
    fun getRanking(auth: String): Pair<Int, String?>
    fun getListRanking(auth: String, page: Int): Pair<Int, String?>
}

interface IRankingPresenter {
    fun getRanking()
    fun getListRanking(page: Int)
}
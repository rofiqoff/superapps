package id.tohaapps.superapps.view.login.module

import okhttp3.FormBody

interface ILoginIteractor {
    fun login(body: FormBody): Pair<Int, String?>
    fun loginSosmed(body: FormBody): Pair<Int, String?>
}

interface ILoginPresenter {
    fun login(mobile: String)
    fun loginSosmed(name: String, email: String)
}
package id.tohaapps.superapps.view.coupon.model

import com.google.gson.annotations.SerializedName

data class VoucherModel(
    @SerializedName("id") var id: String?,
    @SerializedName("kode") var kode: String?,
    @SerializedName("status") var status: String?,
    @SerializedName("judul") var judul: String?,
    @SerializedName("deskripsi") var deskripsi: String?,
    @SerializedName("img") var img: String?,
    @SerializedName("ikon") var ikon: String?,
    @SerializedName("ketentuan") var ketentuan: String?,
    @SerializedName("cara_pakai") var cara_pakai: String?,
    @SerializedName("poin_minimal") var poin_minimal: String?,
    @SerializedName("tanggal_berlaku") var tanggal_berlaku: String?
)
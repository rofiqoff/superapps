package id.tohaapps.superapps.view.home.item.home

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.animation.DecelerateInterpolator
import at.huber.youtubeExtractor.VideoMeta
import at.huber.youtubeExtractor.YouTubeExtractor
import at.huber.youtubeExtractor.YtFile
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.tohaapps.superapps.R
import id.tohaapps.superapps.support.*
import id.tohaapps.superapps.support.exoplayersupport.CallBacks
import id.tohaapps.superapps.support.exoplayersupport.ExoPlayerManagerSupport
import id.tohaapps.superapps.view.home.item.home.listener.HomeListener
import id.tohaapps.superapps.view.home.item.home.model.IklanModel
import kotlinx.android.synthetic.main.item_bottomsheet_play_video.*
import kotlinx.android.synthetic.main.item_bottomsheet_play_video.view.*

class BottomSheetPlayVideo : BottomSheetDialogFragment(), CallBacks.playerChangeListener {

    private val exoPlayerManager by lazy { ExoPlayerManagerSupport.getSharedInstance(activity) }
    private val animator by lazy {
        ObjectAnimator.ofInt(
            rootView.progressBar_time, "Progress", 0, 100
        )
    }

    private var countDownTimer: CountDownTimer? = null

    private val TAG = "ExoPlayerManagerSupport"

    lateinit var rootView: View

    private var urlVideo = ""
    private var iklanId = ""
    private var point = ""
    private var title = ""
    private var isWatched: Boolean? = false

    var data: IklanModel? = null

    companion object {
        var extraData = "extra.data"

        var listener: HomeListener? = null

        fun newInstance(
            data: IklanModel, listener: HomeListener
        ): BottomSheetPlayVideo {
            val fragment = BottomSheetPlayVideo()

            this.listener = listener

            val bundle = Bundle().apply {
                putParcelable(extraData, data)
            }

            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        data = arguments?.getParcelable(extraData)
    }

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        BottomSheetDialog(requireContext(), theme)

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)

        rootView = LayoutInflater.from(context).inflate(R.layout.item_bottomsheet_play_video, null)
        dialog.setContentView(rootView)

        iklanId = data?.id.toString()
        point = data?.poin.toString()
        urlVideo = data?.url.toString()
        title = data?.judul.toString()

        isWatched = data?.watched

        rootView.text_title?.text = title
        rootView.text_message_point?.text = "Selamat! Poin Anda bertambah $point poin"

        if (isWatched == true) {
            rootView.text_message_point?.text =
                "Terimakasih telah menonton iklan kami, tonton kembali besok untuk mendapatkan point"

            rootView.parent_message?.visible()
            rootView.container_timer?.gone()
        }

        rootView.open_youtube?.setOnClickListener { activity?.openWebPage(urlVideo) }

        progressBar?.changeColor(Color.WHITE)

        var url = urlVideo

        if (!urlVideo.startsWith("https://") || urlVideo.startsWith("http://")) {
            url = "https://$urlVideo"
        }

        extractYoutubeUrl(url)

        exoPlayerManager.setPlayerListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        stopCountDown()
    }

    override fun onStop() {
        super.onStop()
        stopVideo()
    }

    override fun onPlayVideo() {
        rootView.progressBar.gone()

        if (countDownTimer == null && isWatched == false) {
            setTimer()
        }
    }

    override fun onIdleVideo() {
        Log.i(TAG, "onIdle")
        rootView.progressBar?.visible()
    }

    override fun onBufferVideo() {
        Log.i(TAG, "onBuffer")
        rootView.progressBar?.visible()
    }

    override fun onEndVideo() {
        rootView.progressBar?.gone()

        Log.i(TAG, "onEnd")
    }

    private fun extractYoutubeUrl(url: String) {
        @SuppressLint("StaticFieldLeak") val mExtractor = object : YouTubeExtractor(activity) {
            override fun onExtractionComplete(
                sparseArray: SparseArray<YtFile>?,
                videoMeta: VideoMeta
            ) {
                if (sparseArray != null) {
                    for (i in 19..100) {
                        showLog("parse index : $i")
                        if (sparseArray.get(i) != null) {
                            showLog("sparseArray : $sparseArray")
                            showLog("sparseArray.get(i) : ${sparseArray.get(i)}")
                            showLog("sparseArray.get(i).url : ${sparseArray.get(i).url}")

                            playVideo(sparseArray.get(i).url)
                            return
                        }
                    }
                }
            }
        }
        mExtractor.extract(url, true, true)
    }

    private fun playVideo(downloadUrl: String?) {
        rootView.mPlayerView?.player = exoPlayerManager.playerView.player
        exoPlayerManager.playStream(downloadUrl)
    }

    private fun stopVideo() {
        exoPlayerManager.stopPlayer(true)
    }

    private fun setTimer() {
        val time = 30

        animator?.duration = (time * 1000).toLong()
        animator?.interpolator = DecelerateInterpolator()
        animator?.start()

        countDownTimer = object : CountDownTimer((time * 1000).toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val t = millisUntilFinished / 1000
                rootView.txt_countdown?.text = t.toString()
            }

            override fun onFinish() {
                rootView.parent_message?.visible()
                rootView.container_timer?.gone()
                listener?.onAddPoint(iklanId, title)
                stopCountDown()
            }
        }.start()
    }

    private fun stopCountDown() {
        if (countDownTimer != null) countDownTimer?.cancel()

        if (animator != null) animator?.cancel()
    }

}
package id.tohaapps.superapps.view.updateprofile.module

import id.tohaapps.superapps.repository.api.MainApi
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.support.*
import okhttp3.FormBody
import org.json.JSONObject

class UpdateProfileIteractor(var api: MainApi) : IUpdateProfileIteractor {
    override fun updateProfile(auth: String, body: FormBody): Pair<Int, String?> {
        return try {
            val response = api.updateProfile("Bearer $auth", body).execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    var code = 0

                    val message = when (response.code()) {
                        404 -> {
                            code = response.code()
                            ERROR_MESSAGE
                        }
                        else -> {
                            val errorBody = response.errorBody()?.string()
                            val json = JSONObject(errorBody)
                            val errorMessage = json.getString("message")

                            code = json.getInt("status_code")

                            when (json.getInt("status_code")) {
                                409 -> ALREADY_EXIST_MESSAGE
                                422 -> MUST_NUMBER_MESSAGE
                                else -> errorMessage
                            }
                        }
                    }

                    Pair(code, message)
                }
            }

        } catch (e: Exception) {
            showLog("failed update profile : ${e.message}")
            Pair(ResponseCode.TIME_OUT, FAILED_MESSAGE)
        }
    }

}
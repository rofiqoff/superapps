package id.tohaapps.superapps.view.home.item.shop.module

import android.content.Context
import id.tohaapps.superapps.repository.AppApiClient
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.repository.data.AppSession
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ShopPresenter(var context: Context, var view: ViewNetworkState) : IShopPresenter {

    private val iteractor by lazy { ShopIteractor(AppApiClient.mainClient()) }

    val getStoreParam = "get.store.param"
    val getPromoParam = "get.promo.param"
    val getTerlarisParam = "get.terlaris.param"

    override fun getStore() {
        view.networkState = NetworkingState.ShowLoading(Pair(getStoreParam, true))
        GlobalScope.launch {
            val response = iteractor.getStore(AppSession(context).profile?.token.toString())

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(getStoreParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(getStoreParam, response.second))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            getStoreParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }

    override fun getPromo() {
        view.networkState = NetworkingState.ShowLoading(Pair(getPromoParam, true))
        GlobalScope.launch {
            val response = iteractor.getPromo(AppSession(context).profile?.token.toString())

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(getPromoParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(getPromoParam, response.second))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            getPromoParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }

    override fun getTerlaris() {
        view.networkState = NetworkingState.ShowLoading(Pair(getTerlarisParam, true))
        GlobalScope.launch {
            val response = iteractor.getTerlaris(AppSession(context).profile?.token.toString())

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(getTerlarisParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(getTerlarisParam, response.second))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            getTerlarisParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }

}
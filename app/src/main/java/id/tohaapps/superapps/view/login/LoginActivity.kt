package id.tohaapps.superapps.view.login

import android.content.Intent
import android.os.Bundle
import com.facebook.*
import com.facebook.login.LoginBehavior
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import id.tohaapps.superapps.R
import id.tohaapps.superapps.repository.IView
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.base.BaseActivity
import id.tohaapps.superapps.repository.data.AppSession
import id.tohaapps.superapps.support.*
import id.tohaapps.superapps.view.home.main.HomeActivity
import id.tohaapps.superapps.view.login.module.LoginPresenter
import id.tohaapps.superapps.view.register.RegisterActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONException

class LoginActivity : BaseActivity(), ViewNetworkState, IView {

    private val RC_SIGN_IN = 864

    private val googleSignInOption by lazy {
        GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .requestProfile()
            .build()
    }

    private val mGoogleSignClient by lazy {
        GoogleSignIn.getClient(
            baseContext, googleSignInOption
        )
    }

    private val loading by lazy { DialogLoading(this) }
    private val presenter by lazy { LoginPresenter(baseContext, this) }
    private lateinit var callbackManager: CallbackManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initView()
    }

    override fun onDestroy() {
        super.onDestroy()
        networkState = NetworkingState.Destroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignResult(task)
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun showLoading(key: String, status: Boolean) {
        super.showLoading(key, status)
        runOnUiThread { if (status) loading.showDialog() else loading.dismissDialog() }
    }

    override fun requestSuccess(key: String, response: Any?) {
        super.requestSuccess(key, response)
        runOnUiThread {
            AppSession(baseContext).isLogin(true)
            showActivityWithClearTop(HomeActivity::class.java)
        }
    }

    override fun requestFailure(key: String, code: Int, message: Any?) {
        super.requestFailure(key, code, message)
        runOnUiThread { showToast(message.toString()) }
    }

    override fun initView() {
        button_sign_in?.setOnClickListener { onSign() }

        button_google?.setOnClickListener { onSignGoogle() }

        button_facebook?.setOnClickListener { onSignFacebook() }

        callbackManager = CallbackManager.Factory.create()

        login_button?.setLoginText("Facebook")
        login_button?.setLogoutText("Keluar")
        login_button?.setPadding(24, 24, 24, 24)

        LoginManager.getInstance().loginBehavior = LoginBehavior.NATIVE_WITH_FALLBACK

        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    showLog("custom button -> success login facebook")
                    showLog("custom button -> result login facebook : ${result?.accessToken}")

                    val accessToken = AccessToken.getCurrentAccessToken()

                    val graphRequest =
                        GraphRequest.newMeRequest(
                            accessToken
                        ) { `object`, response ->
                            try {
                                showLog("fb json object: $`object`")
                                showLog("fb graph response: $response")

                                val name = `object`?.getString("name")

                                var email = ""
                                if (`object`?.has("email") == true) {
                                    email = `object`.getString("email")
                                }

                                showLog("facebook profile : $name, $email")

                                requestLoginSosmed(name, email)

                            } catch (e: JSONException) {
                                e.printStackTrace()
                                showToast("Terjadi kesalahan, silahkan coba kembali")
                                showLog("error graph facebook login : ${e.printStackTrace()}")
                            }
                        }

                    val parameters = Bundle()
                    parameters.putString("fields", "id,name,email")
                    graphRequest.parameters = parameters
                    graphRequest.executeAsync()
                }

                override fun onCancel() {
                    showLog("custom button -> cancel login facebook")
                }

                override fun onError(error: FacebookException?) {
                    showLog("custom button -> error login facebook : ${error?.message}")
                }
            })

        text_register?.setOnClickListener { onRegister() }
    }

    private fun onSign() {
        val regexStr = "^[0-9]*$".toRegex()

        val inputText = edit_input?.text.toString().trim()

        if (inputText.isEmpty()) {
            showToast(getString(R.string.message_empty_input_login))
            return
        }

        showLog("phone : $inputText")

        presenter.login(inputText)

//            val typeInput = if (inputText.matches(regexStr)) {
//                if (inputText.startsWith("0")) {
//                    if (inputText.length > 8) {
//                        VerificationActivity.PARAM_TYPE_PHONE
//                    } else {
//                        showToast(getString(R.string.message_toast_error_input_login))
//                        return@setOnClickListener
//                    }
//                } else {
//                    showToast(getString(R.string.message_toast_error_input_login))
//                    return@setOnClickListener
//                }
//            } else if (inputText.startsWith("+62")) {
//                if (inputText.length > 10) {
//                    VerificationActivity.PARAM_TYPE_PHONE
//                } else {
//                    showToast(getString(R.string.message_toast_error_input_login))
//                    return@setOnClickListener
//                }
//            } else {
//                if (isValidEmail(inputText)) {
//                    VerificationActivity.PARAM_TYPE_EMAIL
//                } else {
//                    showToast(getString(R.string.message_toast_error_input_login))
//                    return@setOnClickListener
//                }
//            }

//            showActivity(
//                VerificationActivity::class.java,
//                VerificationActivity.PARAM_TYPE, typeInput,
//                VerificationActivity.PARAM_TEXT, inputText
//            )
    }

    private fun onRegister() {
        showActivity(RegisterActivity::class.java)
    }

    private fun onSignGoogle() {
        startActivityForResult(mGoogleSignClient.signInIntent, RC_SIGN_IN)
    }

    private fun onSignFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, listOf("public_profile", "email"))
    }

    private fun getAccountGmail(account: GoogleSignInAccount?) {
        val personName = account?.displayName
        val personGivenName = account?.givenName
        val personFamilyName = account?.familyName
        val personEmail = account?.email
        val personId = account?.id
        val personPhoto = account?.photoUrl

        showLog("personName: $personName")
        showLog("personGivenName: $personGivenName")
        showLog("personFamilyName: $personFamilyName")
        showLog("personEmail: $personEmail")
        showLog("personId: $personId")
        showLog("personPhoto: $personPhoto")

        requestLoginSosmed(personName, personEmail)

    }

    private fun handleSignResult(completeTask: Task<GoogleSignInAccount>) {
        try {
            val account = completeTask.getResult(ApiException::class.java)
            getAccountGmail(account)
        } catch (e: ApiException) {
            showToast("Terjadi kesalahan, silahkan coba kembali")
            showLog("signInResult: failed code = ${e.statusCode}")
//            getAccountGmail(null)
        }
    }

    private fun requestLoginSosmed(name: String?, email: String?) {
        presenter.loginSosmed(name.toString(), email.toString())
    }

}

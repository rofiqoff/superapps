package id.tohaapps.superapps.view.home.item.shop

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.tohaapps.superapps.R
import id.tohaapps.superapps.repository.IView
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.base.BaseFragment
import id.tohaapps.superapps.support.*
import id.tohaapps.superapps.support.adapter.Adapter
import id.tohaapps.superapps.view.home.item.shop.model.ProductModel
import id.tohaapps.superapps.view.home.item.shop.model.StoreModel
import id.tohaapps.superapps.view.home.item.shop.module.ShopPresenter
import id.tohaapps.superapps.view.home.item.shop.viewholder.PromoViewHolder
import id.tohaapps.superapps.view.home.item.shop.viewholder.StoreViewHolder
import id.tohaapps.superapps.view.home.item.shop.viewholder.TerlarisViewHolder
import kotlinx.android.synthetic.main.fragment_shop.*

class ShopFragment : BaseFragment(), ViewNetworkState,
    IView {

    private val presenter by lazy { context?.let { ShopPresenter(it, this) } }
    private var isRefresh = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this item
        return inflater.inflate(R.layout.fragment_shop, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onDestroy() {
        super.onDestroy()
        networkState = NetworkingState.Destroy()
    }

    override fun showLoading(key: String, status: Boolean) {
        super.showLoading(key, status)
        activity?.runOnUiThread {
            swipe_refresh?.apply { if (status) show() else hide() }

            when (key) {
                presenter?.getPromoParam -> loading_promo?.apply { if (status) visible() else gone() }
                presenter?.getTerlarisParam -> loading_terlaris?.apply { if (status) visible() else gone() }
                presenter?.getStoreParam -> loading_store?.apply { if (status) visible() else gone() }
            }
        }
    }

    override fun requestSuccess(key: String, response: Any?) {
        super.requestSuccess(key, response)
        activity?.runOnUiThread {
            when (key) {
                presenter?.getPromoParam -> {
                    val STATUS_TYPE_Object =
                        object : TypeToken<List<ProductModel>>() {}.type
                    val listData = Gson().fromJson<List<ProductModel>>(
                        response.toString(), STATUS_TYPE_Object
                    )
                    initListPromo(listData as ArrayList<ProductModel>)
                }
                presenter?.getTerlarisParam -> {
                    val STATUS_TYPE_Object =
                        object : TypeToken<List<ProductModel>>() {}.type
                    val listData = Gson().fromJson<List<ProductModel>>(
                        response.toString(), STATUS_TYPE_Object
                    )
                    initListTerlaris(listData as ArrayList<ProductModel>)
                }
                presenter?.getStoreParam -> {
                    val STATUS_TYPE_Object = object : TypeToken<List<StoreModel>>() {}.type
                    val listData = Gson().fromJson<List<StoreModel>>(
                        response.toString(), STATUS_TYPE_Object
                    )
                    initListStore(listData as ArrayList<StoreModel>)
                }
            }
        }
    }

    override fun requestFailure(key: String, code: Int, message: Any?) {
        super.requestFailure(key, code, message)
        activity?.runOnUiThread {
            activity?.showToast(message.toString())

            when (key) {
                presenter?.getPromoParam -> {
                }
                presenter?.getTerlarisParam -> {
                }
                presenter?.getStoreParam -> {
                }
            }
        }
    }

    override fun initView() {
        requestData()

        swipe_refresh?.apply {
            setColorSchemeResources(R.color.colorGreen, R.color.colorOrange, R.color.colorRed)
            setOnRefreshListener {
                swipe_refresh?.hide()
                requestData(true)
            }
        }
    }

    private fun requestData(isRefresh: Boolean = false) {
        this.isRefresh = isRefresh

        presenter?.getStore()
        presenter?.getPromo()
        presenter?.getTerlaris()
    }

    private fun initListStore(dataList: ArrayList<StoreModel>) {
        list_store?.apply {
            adapter = object : Adapter<StoreModel, StoreViewHolder>(
                R.layout.row_content_shop_store,
                StoreViewHolder::class.java, StoreModel::class.java, dataList
            ) {
                override fun bindView(holder: StoreViewHolder, model: StoreModel, position: Int) {
                    holder.onBind(activity, model)
                }
            }

            layoutManager = GridLayoutManager(context, 3)
            isNestedScrollingEnabled = false
        }
    }

    private fun initListPromo(dataList: ArrayList<ProductModel>) {
        list_promo?.apply {
            adapter = object : Adapter<ProductModel, PromoViewHolder>(
                R.layout.row_content_shop_promo,
                PromoViewHolder::class.java, ProductModel::class.java, dataList
            ) {
                override fun bindView(holder: PromoViewHolder, model: ProductModel, position: Int) {
                    holder.onBind(model)
                }
            }

            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            isNestedScrollingEnabled = false
        }
    }

    private fun initListTerlaris(dataList: ArrayList<ProductModel>) {
        list_product_terlaris?.apply {
            adapter = object : Adapter<ProductModel, TerlarisViewHolder>(
                R.layout.row_content_shop_terlaris,
                TerlarisViewHolder::class.java, ProductModel::class.java, dataList
            ) {
                override fun bindView(
                    holder: TerlarisViewHolder,
                    model: ProductModel,
                    position: Int
                ) {
                    holder.onBind(model)
                }
            }

            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            isNestedScrollingEnabled = false
        }
    }
}

package id.tohaapps.superapps.view.ranking.viewholder

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import id.tohaapps.superapps.R
import id.tohaapps.superapps.support.formatPoint
import id.tohaapps.superapps.view.ranking.model.ItemRangkingModel
import kotlinx.android.synthetic.main.item_list_ranking_all.view.*

class RankingAllViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
    fun onBind(data: ItemRangkingModel?, myRanking: String?) {
        val no = data?.no
        val name = data?.name
        val point = data?.poin

        itemView.text_number?.text = no
        itemView.text_name?.text = name
        itemView.text_point?.text = formatPoint(point)

        if (myRanking == no) {
            itemView.root?.setBackgroundColor(
                ContextCompat.getColor(
                    itemView.context,
                    R.color.colorBlueSoftBackground
                )
            )
        }

    }
}
package id.tohaapps.superapps.view.home.item.event.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_content_schedule_event.view.*

class ScheduleEventViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun onBind(data: String) {
        itemView.text_item?.text = data
    }
}
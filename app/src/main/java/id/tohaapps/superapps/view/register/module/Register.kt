package id.tohaapps.superapps.view.register.module

import okhttp3.FormBody

interface IRegisterIteractor {
    fun register(body: FormBody): Pair<Int, String?>
}

interface IRegisterPresenter {
    fun register(name: String, mobile: String)
}
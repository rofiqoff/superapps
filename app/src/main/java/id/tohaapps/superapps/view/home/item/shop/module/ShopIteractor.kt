package id.tohaapps.superapps.view.home.item.shop.module

import id.tohaapps.superapps.repository.api.MainApi
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.support.ERROR_MESSAGE
import id.tohaapps.superapps.support.FAILED_LOGOUT
import id.tohaapps.superapps.support.FAILED_MESSAGE
import id.tohaapps.superapps.support.showLog

class ShopIteractor(var api: MainApi) : IShopIteractor {
    override fun getStore(auth: String): Pair<Int, String?> {
        return try {
            val response = api.getShop("Bearer $auth").execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    val message = when (response.code()) {
                        401 -> FAILED_LOGOUT
                        else -> ERROR_MESSAGE
                    }

                    Pair(response.code(), message)
                }
            }

        } catch (e: Exception) {
            showLog("failed get store : ${e.message}")
            Pair(ResponseCode.TIME_OUT, FAILED_MESSAGE)
        }
    }

    override fun getPromo(auth: String): Pair<Int, String?> {
        return try {
            val response = api.getProductPromo("Bearer $auth").execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    val code = response.code()

                    val message = if (code == 401) FAILED_LOGOUT else ERROR_MESSAGE

                    Pair(code, message)
                }
            }

        } catch (e: Exception) {
            showLog("failed get promo : ${e.message}")
            Pair(ResponseCode.TIME_OUT, FAILED_MESSAGE)
        }
    }

    override fun getTerlaris(auth: String): Pair<Int, String?> {
        return try {
            val response = api.getProductLaris("Bearer $auth").execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    val code = response.code()

                    val message = if (code == 401) FAILED_LOGOUT else ERROR_MESSAGE

                    Pair(code, message)
                }
            }

        } catch (e: Exception) {
            showLog("failed get laris : ${e.message}")
            Pair(ResponseCode.TIME_OUT, FAILED_MESSAGE)
        }
    }

}
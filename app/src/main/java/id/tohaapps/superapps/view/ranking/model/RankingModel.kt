package id.tohaapps.superapps.view.ranking.model

import com.google.gson.annotations.SerializedName

data class RankingModel(
    @SerializedName("ranking") var ranking: ArrayList<ItemRangkingModel>?,
    @SerializedName("sendiri") var sendiri: ItemRangkingModel?
)

data class ListRankingModel(
    @SerializedName("data") var data: ArrayList<ItemRangkingModel>?,
    @SerializedName("self") var self: ItemRangkingModel?,
    @SerializedName("per_page") var per_page: Int?,
    @SerializedName("current_page") var current_page: Int?,
    @SerializedName("last_page") var last_page: Int?
)

data class ItemRangkingModel(
    @SerializedName("no") var no: String?,
    @SerializedName("name") var name: String?,
    @SerializedName("poin") var poin: String?
)
package id.tohaapps.superapps.view.home.item.explore.listener

import android.graphics.Bitmap

interface onUploadImageListener {
    fun onAddImage()
    fun onDeleteImage(position: Int)
}

interface onUpdateImageListener {
    fun onUpdateImage(bitmap: Bitmap)
}
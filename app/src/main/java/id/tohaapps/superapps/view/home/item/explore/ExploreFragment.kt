package id.tohaapps.superapps.view.home.item.explore

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.tohaapps.superapps.R
import id.tohaapps.superapps.repository.IView
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.base.BaseFragment
import id.tohaapps.superapps.support.*
import id.tohaapps.superapps.support.adapter.Adapter
import id.tohaapps.superapps.view.home.item.explore.model.StatusModel
import id.tohaapps.superapps.view.home.item.explore.module.ExplorePresenter
import id.tohaapps.superapps.view.home.item.explore.viewholder.ExploreViewHolder
import kotlinx.android.synthetic.main.fragment_explore.*

class ExploreFragment : BaseFragment(), ViewNetworkState,
    IView {

    private val presenter by lazy { context?.let { ExplorePresenter(it, this) } }
    private var isRefresh = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this item
        return inflater.inflate(R.layout.fragment_explore, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onResume() {
        super.onResume()
        requestExplore()
    }

    override fun onDestroy() {
        super.onDestroy()
        networkState = NetworkingState.Destroy()
    }

    override fun showLoading(key: String, status: Boolean) {
        super.showLoading(key, status)
        activity?.runOnUiThread {
            when (key) {
                presenter?.getStatusParam -> {
                    if (isRefresh) {
                        swipe_refresh?.apply { if (status) show() else hide() }
                    } else {
                        if (status) {
                            swipe_refresh?.disable()
                            loading?.visible()
                            parent_list?.gone()
                        } else {
                            swipe_refresh?.enable()
                            loading?.gone()
                            parent_list?.visible()
                        }
                    }
                }
            }
        }
    }

    override fun requestSuccess(key: String, response: Any?) {
        super.requestSuccess(key, response)
        activity?.runOnUiThread {
            when (key) {
                presenter?.getStatusParam -> {
                    val STATUS_TYPE_Object = object : TypeToken<List<StatusModel>>() {}.type
                    val listData = Gson().fromJson<List<StatusModel>>(
                        response.toString(), STATUS_TYPE_Object
                    )
                    initList(listData as ArrayList<StatusModel>)
                }
            }
        }
    }

    override fun requestFailure(key: String, code: Int, message: Any?) {
        super.requestFailure(key, code, message)
        activity?.runOnUiThread {
            when (key) {
                presenter?.getStatusParam -> {
                    activity?.showToast(message.toString())
                }
            }
        }
    }

    override fun initView() {

        fab_create_post?.setOnClickListener {
            //            activity?.showToast(getString(R.string.message_feature_coming_soon))
            activity?.showActivity(CreatePostActivity::class.java)
        }

        swipe_refresh?.apply {
            setColorSchemeResources(R.color.colorGreen, R.color.colorOrange, R.color.colorRed)
            setOnRefreshListener { requestExplore(true) }
        }
    }

    private fun requestExplore(isRefresh: Boolean = false) {
        this.isRefresh = isRefresh
        presenter?.getStatus()
    }

    private fun initList(dataList: ArrayList<StatusModel>) {
        val adapterList = object : Adapter<StatusModel, ExploreViewHolder>(
            R.layout.row_content_explore,
            ExploreViewHolder::class.java,
            StatusModel::class.java,
            dataList
        ) {
            override fun bindView(holder: ExploreViewHolder, model: StatusModel, position: Int) {
                holder.onBind(model)
            }
        }

        list?.adapter = adapterList
        list?.layoutManager = LinearLayoutManager(context)

    }

}

package id.tohaapps.superapps.view.home.item.event.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import id.tohaapps.superapps.R;
import id.tohaapps.superapps.support.SupportObject;
import id.tohaapps.superapps.view.home.item.event.EventDetailActivity;
import id.tohaapps.superapps.view.home.item.event.model.EventModel;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> implements RecyclerSectionItemDecoration.SectionCallback {

    private final List<EventModel> people;
    private final LayoutInflater layoutInflater;
    private final int rowLayout;
    private final Context mContext;

    public EventAdapter(Context context, LayoutInflater layoutInflater, List<EventModel> people, @LayoutRes int rowLayout) {
        mContext = context;
        this.people = people;
        this.layoutInflater = layoutInflater;
        this.rowLayout = rowLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(rowLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        EventModel person = people.get(position);
        holder.title.setText(person.getJudul());
        holder.location.setText(person.getLokasi());
        holder.participant.setText(person.getPeserta());
        Glide.with(mContext).load(person.getImg())
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(holder.image);

        holder.root.setOnClickListener(view1 -> {
            Intent intent = new Intent(view1.getContext(), EventDetailActivity.class);
            intent.putExtra("data", person);
            view1.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return people.size();
    }

    @Override
    public boolean isSection(int position) {
        boolean result = position == 0
                || !SupportObject.INSTANCE.parseTimeUTC(people.get(position).getWaktu(),
                "yyyy-MM-dd HH:mm:ss", "EEEE, dd MMMM yyyy")
                .equals(SupportObject.INSTANCE.parseTimeUTC(people.get(position - 1).getWaktu(),
                        "yyyy-MM-dd HH:mm:ss", "EEEE, dd MMMM yyyy"));
        return result;
    }

    @Override
    public CharSequence getSectionHeader(int position) {
        return SupportObject.INSTANCE.dateFormat(people.get(position).getWaktu());
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView title;
        private AppCompatTextView location;
        private AppCompatTextView participant;
        private AppCompatImageView image;
        private View line;
        private ConstraintLayout root;

        public ViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.text_title);
            location = view.findViewById(R.id.text_location);
            participant = view.findViewById(R.id.text_participant);
            image = view.findViewById(R.id.image_event);
            line = view.findViewById(R.id.line);
            root = view.findViewById(R.id.root);
        }
    }
}

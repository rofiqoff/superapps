package id.tohaapps.superapps.view.updateprofile.module

import android.content.Context
import id.tohaapps.superapps.repository.AppApiClient
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.repository.data.AppSession
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.FormBody

class UpdateProfilePresenter(var context: Context, var view: ViewNetworkState) :
    IUpdateProfilePresenter {

    private val iteractor by lazy { UpdateProfileIteractor(AppApiClient.mainClient()) }

    val updateProfileParam = "update.profile.param"

    override fun updateProfile(name: String, mobile: String) {
        view.networkState = NetworkingState.ShowLoading(Pair(updateProfileParam, true))
        GlobalScope.launch {

            val formBody = FormBody.Builder()
                .add("name", name)
                .add("mobile", mobile)
                .build()

            val response =
                iteractor.updateProfile(AppSession(context).profile?.token.toString(), formBody)
            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(updateProfileParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(updateProfileParam, "success"))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            updateProfileParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }
}
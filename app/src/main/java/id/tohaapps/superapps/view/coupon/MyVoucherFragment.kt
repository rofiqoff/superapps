package id.tohaapps.superapps.view.coupon

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.tohaapps.superapps.R
import id.tohaapps.superapps.repository.IView
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.base.BaseFragment
import id.tohaapps.superapps.support.*
import id.tohaapps.superapps.support.adapter.Adapter
import id.tohaapps.superapps.view.coupon.listener.MyVoucherCountListener
import id.tohaapps.superapps.view.coupon.listener.VoucherClickListener
import id.tohaapps.superapps.view.coupon.model.VoucherModel
import id.tohaapps.superapps.view.coupon.module.VoucherPresenter
import id.tohaapps.superapps.view.coupon.viewholder.CouponViewHolder
import kotlinx.android.synthetic.main.fragment_voucher.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MyVoucherFragment : BaseFragment(), VoucherClickListener, IView,
    ViewNetworkState {

    private val presenter by lazy { context?.let { VoucherPresenter(it, this) } }
    private var isRefresh = false

    companion object {
        lateinit var listener: MyVoucherCountListener

        fun newInstance(myListener: MyVoucherCountListener): MyVoucherFragment {
            val fragment = MyVoucherFragment()
            listener = myListener
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this item
        return inflater.inflate(R.layout.fragment_voucher, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            GlobalScope.launch {
                delay(1000)
                swipeProgress()
            }
        }
    }

    override fun showLoading(key: String, status: Boolean) {
        super.showLoading(key, status)
        activity?.runOnUiThread {
            when (key) {
                presenter?.myVoucherParam -> {
                    if (isRefresh) {
                        swipe_refresh?.apply { if (status) show() else hide() }
                    } else {
                        if (status) {
                            swipe_refresh?.disable()
                            loading?.visible()
                            list?.gone()
                        } else {
                            swipe_refresh?.enable()
                            loading?.gone()
                            list?.visible()
                        }
                    }
                }
            }
        }
    }

    override fun requestSuccess(key: String, response: Any?) {
        super.requestSuccess(key, response)
        activity?.runOnUiThread {
            when (key) {
                presenter?.myVoucherParam -> {
                    val statusObjectType =
                        object : TypeToken<ArrayList<VoucherModel>>() {}.type
                    val listData = Gson().fromJson<ArrayList<VoucherModel>>(
                        response.toString(), statusObjectType
                    )

                    if (listData.isEmpty()) {
                        layout_empty?.visible()
                        list?.gone()
                    } else {
                        initListVoucher(listData)
                    }

                }
            }
        }
    }

    override fun requestFailure(key: String, code: Int, message: Any?) {
        super.requestFailure(key, code, message)
        activity?.runOnUiThread {
            when (key) {
                presenter?.myVoucherParam -> {
                    activity?.showToast(message.toString())
                }
            }
        }
    }

    override fun initView() {
        swipe_refresh?.apply {
            setColorSchemeResources(R.color.colorGreen, R.color.colorOrange, R.color.colorRed)
            setOnRefreshListener { requestVoucher(true) }
        }

        swipeProgress()
    }

    override fun onClickVoucher(data: VoucherModel) {
        BottomSheetVoucherDetail.newInstance(data, true).show(childFragmentManager, "show detail")
    }

    private fun initListVoucher(dataList: ArrayList<VoucherModel>) {
        layout_empty?.gone()
        list?.visible()

        listener.countMyVoucher(dataList.size)

        list?.apply {
            adapter = object : Adapter<VoucherModel, CouponViewHolder>(
                R.layout.row_content_coupon,
                CouponViewHolder::class.java, VoucherModel::class.java, dataList
            ) {
                override fun bindView(
                    holder: CouponViewHolder,
                    model: VoucherModel,
                    position: Int
                ) {
                    holder.onBind(model, this@MyVoucherFragment)
                }
            }

            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
        }

    }

    private fun requestVoucher(isRefresh: Boolean = false) {
        this.isRefresh = isRefresh

        presenter?.getMyVoucher()
    }

    private fun swipeProgress() {
        swipe_refresh?.post { requestVoucher(true) }
    }

}

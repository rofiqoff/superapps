package id.tohaapps.superapps.view.login.module

import id.tohaapps.superapps.repository.api.MainApi
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.support.FAILED_MESSAGE
import id.tohaapps.superapps.support.MUST_NUMBER_MESSAGE
import id.tohaapps.superapps.support.NOT_FOUND_MESSAGE
import id.tohaapps.superapps.support.showLog
import okhttp3.FormBody
import org.json.JSONObject

class LoginIteractor(var api: MainApi) : ILoginIteractor {
    override fun loginSosmed(body: FormBody): Pair<Int, String?> {
        return try {
            val response = api.signInSosmed(body).execute()
            when (response.isSuccessful) {

                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    val errorBody = response.errorBody()?.string()
                    val json = JSONObject(errorBody)
                    val errorMessage = json.getString("message")

                    val statusCode = json.getInt("status_code")

                    when (statusCode) {
                        404 -> Pair(statusCode, NOT_FOUND_MESSAGE)
                        422 -> Pair(statusCode, MUST_NUMBER_MESSAGE)
                        else -> Pair(statusCode, errorMessage)
                    }
                }
            }

        } catch (e: Exception) {
            showLog("failed login sosmed : ${e.message}")
            Pair(ResponseCode.TIME_OUT, FAILED_MESSAGE)
        }
    }

    override fun login(body: FormBody): Pair<Int, String?> {
        return try {
            val response = api.signIn(body).execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    val errorBody = response.errorBody()?.string()
                    val json = JSONObject(errorBody)
                    val errorMessage = json.getString("message")

                    val statusCode = json.getInt("status_code")

                    when (statusCode) {
                        404 -> Pair(statusCode, NOT_FOUND_MESSAGE)
                        422 -> Pair(statusCode, MUST_NUMBER_MESSAGE)
                        else -> Pair(statusCode, errorMessage)
                    }
                }
            }

        } catch (e: Exception) {
            showLog("failed login : ${e.message}")
            Pair(ResponseCode.TIME_OUT, FAILED_MESSAGE)
        }
    }

}
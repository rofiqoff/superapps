package id.tohaapps.superapps.view.coupon.listener

import id.tohaapps.superapps.view.coupon.model.VoucherModel

interface VoucherClickListener {
    fun onClickVoucher(data: VoucherModel)
}

interface VoucherReedemListener{
    fun onReedemSuccess(point: Int)
    fun onReedemFailure(message: String?)
}

interface MyVoucherCountListener{
    fun countMyVoucher(count: Int)
}

interface MyVoucherClickListener{
    fun onShowMyVoucher()
}
package id.tohaapps.superapps.view.home.item.explore.viewholder

import android.graphics.Bitmap
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import id.tohaapps.superapps.support.displayImage
import id.tohaapps.superapps.view.home.item.explore.listener.onUploadImageListener
import kotlinx.android.synthetic.main.row_list_image_status.view.*

class UploadImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun onBind(bitmap: Bitmap, listener: onUploadImageListener, position: Int) {
        itemView.image?.displayImage(itemView.context, bitmap)

        itemView.btn_upload?.setOnClickListener { listener.onAddImage() }

        itemView.btn_close?.setOnClickListener { listener.onDeleteImage(position) }
    }
}
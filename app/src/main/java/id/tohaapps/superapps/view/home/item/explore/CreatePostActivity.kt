package id.tohaapps.superapps.view.home.item.explore

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import id.tohaapps.superapps.R
import id.tohaapps.superapps.repository.IView
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.base.BaseActivity
import id.tohaapps.superapps.support.*
import id.tohaapps.superapps.support.adapter.Adapter
import id.tohaapps.superapps.support.customview.ImageChooser
import id.tohaapps.superapps.view.home.item.explore.listener.onUploadImageListener
import id.tohaapps.superapps.view.home.item.explore.module.ExplorePresenter
import id.tohaapps.superapps.view.home.item.explore.viewholder.UploadImageViewHolder
import kotlinx.android.synthetic.main.activity_create_pot.*
import kotlinx.android.synthetic.main.row_list_image_status.view.*
import org.json.JSONObject
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException

class CreatePostActivity : BaseActivity(), onUploadImageListener,
    ViewNetworkState, IView {

    private val presenter by lazy { ExplorePresenter(this, this) }
    private val imageChooser by lazy { ImageChooser(this, this) }
    private val loading by lazy { DialogLoading(this) }

    private var adapterImage: Adapter<Bitmap, UploadImageViewHolder>? = null

    private var imageFile: ArrayList<File>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_pot)
        initView()
    }

    override fun onDestroy() {
        super.onDestroy()
        networkState = NetworkingState.Destroy()
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        changeColorTextMenu()
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_create_post, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_send -> {
                if (edt_input?.text.toString().isNotEmpty()) {
                    requestCreateStatus()
                } else {
                    showToast("Setidaknya Anda menuliskan beberapa kata untuk di posting :)")
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK) {
            return
        }

        if (data != null) {
            if (requestCode == ImageChooser.RESULT_CODE_CAMERA) {
                text_upload?.gone()

                val thumbnail = data.extras?.get("data") as Bitmap
                storeImage(thumbnail)?.let { imageFile?.add(it) }

                if (adapterImage == null) {
                    initListFoto(thumbnail)
                } else {
                    if (adapterImage?.itemCount == 0) {
                        adapterImage?.addData(thumbnail)
                    } else {
                        adapterImage?.insertData(
                            adapterImage?.itemCount.toString().toInt() - 1,
                            thumbnail
                        )
                    }
                }

//                if (adapterImage?.itemCount != 0) {
//                    if (adapterImage == null) {
//                        initListFoto(thumbnail)
//                    } else {
//                        adapterImage?.insertData(
//                            adapterImage?.itemCount.toString().toInt() - 1,
//                            thumbnail
//                        )
//                    }
//                } else {
//                    adapterImage?.addData(thumbnail)
//                }

            }

            if (requestCode == ImageChooser.RESULT_CODE_GALLERY) {
                text_upload?.gone()

                try {
                    val uri = data.data
                    val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uri)

                    storeImage(bitmap)?.let { imageFile?.add(it) }

                    if (adapterImage == null) {
                        initListFoto(bitmap)
                    } else {
                        if (adapterImage?.itemCount == 0) {
                            adapterImage?.addData(bitmap)
                        } else {
                            adapterImage?.insertData(
                                adapterImage?.itemCount.toString().toInt() - 1,
                                bitmap
                            )
                        }
                    }

//                    if (adapterImage?.itemCount != 0) {
//                        if (adapterImage == null) {
//                            initListFoto(bitmap)
//                        } else {
//                            adapterImage?.insertData(
//                                adapterImage?.itemCount.toString().toInt() - 1,
//                                bitmap
//                            )
//                        }
//                    } else {
//                        adapterImage?.addData(bitmap)
//                    }

                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray
    ) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == ImageChooser.PERMISSION_CODE_CAMERA) {
                imageChooser.Camera()
            } else if (requestCode == ImageChooser.PERMIISION_CODE_WRITE2) {
                imageChooser.Gallery()
            }
        }
    }

    override fun onAddImage() {
        imageChooser.ImageChooserDialog("Unggah Gambar Dari")
    }

    override fun onDeleteImage(position: Int) {
        adapterImage?.remove(position)
        imageFile?.removeAt(position)

        if (adapterImage?.itemCount == 0) {
            text_upload?.visible()
        }
    }

    override fun showLoading(key: String, status: Boolean) {
        super.showLoading(key, status)
        runOnUiThread {
            when (key) {
                presenter.createStatusParam -> if (status) loading.showDialog() else loading.dismissDialog()
            }
        }
    }

    override fun requestSuccess(key: String, response: Any?) {
        super.requestSuccess(key, response)
        runOnUiThread {
            when (key) {
                presenter.createStatusParam -> {
                    val body = JSONObject(response.toString())
                    val status_code = body.getInt("status_code")
                    val message = body.getString("message")

                    if (status_code == 200) {
                        showToast("Anda berhasil membuat status")
                        onBackPressed()
                    } else {
                        showToast(message)
                    }

                }
            }
        }
    }

    override fun requestFailure(key: String, code: Int, message: Any?) {
        super.requestFailure(key, code, message)
        runOnUiThread {
            when (key) {
                presenter.createStatusParam -> showToast(message.toString())
            }
        }
    }

    override fun initView() {
        initToolbar()
        invalidateOptionsMenu()
        config()

        imageFile = ArrayList<File>()

        text_upload?.setOnClickListener { imageChooser.ImageChooserDialog("Unggah Gambar Dari") }
    }

    private fun initToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun getOutputMediaFile(): File? {
        val mediaStorageDir = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).toString()
                    + "/enesis"
        )

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null
            }
        }

        val mediaFile: File
        val mImageName = "capture${System.currentTimeMillis()}.jpg"
        mediaFile = File(mediaStorageDir.path + File.separator + mImageName)
        return mediaFile
    }

    private fun removeFileInDirectory() {

    }

    private fun storeImage(image: Bitmap): File? {
        val pictureFile = getOutputMediaFile() ?: return null
        try {
            val fos = FileOutputStream(pictureFile)
            image.compress(Bitmap.CompressFormat.WEBP, 100, fos)
            fos.apply { flush(); close() }

        } catch (e: FileNotFoundException) {
            showLog("TAGCreate -> File not found: " + e.message)
        } catch (e: IOException) {
            showLog("TAGCreate -> Error accessing file: " + e.message)
        }
        return pictureFile
    }

    private fun config() {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun changeColorTextMenu() {
        val view = findViewById<View>(R.id.menu_send)
        if (view != null && view is TextView) {
            view.setTextColor(ContextCompat.getColor(baseContext, R.color.colorAccent))
        }
    }

    private fun initListFoto(bitmap: Bitmap) {
        val data = ArrayList<Bitmap>()
        data.add(bitmap)

        adapterImage = object : Adapter<Bitmap, UploadImageViewHolder>(
            R.layout.row_list_image_status,
            UploadImageViewHolder::class.java,
            Bitmap::class.java,
            data
        ) {
            override fun bindView(holder: UploadImageViewHolder, model: Bitmap, position: Int) {
                holder.onBind(model, this@CreatePostActivity, position)

                if (position >= adapterImage?.itemCount.toString().toInt() - 1) {
                    holder.itemView.btn_upload?.visible()
                } else {
                    holder.itemView.btn_upload?.gone()
                }
            }
        }

        list_image?.adapter = adapterImage
        list_image?.layoutManager =
            LinearLayoutManager(baseContext, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun requestCreateStatus() {
        val status = edt_input?.text.toString()
        val waktu = dateTimeNow("yyyy-MM-dd HH:mm:ss")

        showLog("requestCreateStatus: $status, $waktu, ${imageFile?.size}")

        presenter.createStatus(status, waktu, imageFile)
    }

}

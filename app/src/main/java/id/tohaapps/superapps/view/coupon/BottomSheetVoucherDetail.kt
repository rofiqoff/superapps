package id.tohaapps.superapps.view.coupon

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.transition.TransitionManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import id.tohaapps.superapps.R
import id.tohaapps.superapps.repository.IView
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.base.BaseBottomSheetFragment
import id.tohaapps.superapps.support.*
import id.tohaapps.superapps.view.coupon.listener.VoucherReedemListener
import id.tohaapps.superapps.view.coupon.model.VoucherModel
import id.tohaapps.superapps.view.coupon.module.VoucherPresenter
import kotlinx.android.synthetic.main.item_bottomsheet_voucher_detail.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.json.JSONObject

class BottomSheetVoucherDetail : BaseBottomSheetFragment(), IView,
    ViewNetworkState {

    private val presenter by lazy { context?.let { VoucherPresenter(it, this) } }
    private var behavior: CoordinatorLayout.Behavior<*>? = null
    private var point = 0
    private lateinit var viewLayout: View

    companion object {
        var dataVoucher: VoucherModel? = null
        var isMyVoucher = false
        private var listenerReedem: VoucherReedemListener? = null

        fun newInstance(
            data: VoucherModel,
            myVoucher: Boolean,
            listener: VoucherReedemListener? = null
        ): BottomSheetVoucherDetail {
            val fragment = BottomSheetVoucherDetail();

            dataVoucher = data
            isMyVoucher = myVoucher
            listenerReedem = listener

            return fragment
        }
    }

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        BottomSheetDialog(requireContext(), theme)

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        viewLayout =
            LayoutInflater.from(context).inflate(R.layout.item_bottomsheet_voucher_detail, null)
        dialog.setContentView(viewLayout)

        val params = (viewLayout.parent as View).layoutParams as CoordinatorLayout.LayoutParams
        behavior = params.behavior

        val expired = parseTimeUTC(
            dataVoucher?.tanggal_berlaku, "yyyy-MM-dd HH:mm:ss", "dd MMMM yyyy"
        )

        point = dataVoucher?.poin_minimal.toString().toInt()

        if (isMyVoucher) {
            viewLayout.parent_code?.visible()
            viewLayout.parent_button?.gone()
            viewLayout.text_point?.gone()
        }

        viewLayout.loading?.changeColor(Color.WHITE)

//        viewLayout.image?.load(dataVoucher?.img)

        val image = dataVoucher?.img

        val imageUrl = if (image?.startsWith("/") == true) {
            val a = image.removeRange(0, 0)
            "$baseUrl$a"
        } else {
            image
        }

        viewLayout.image?.displayImage(context, imageUrl)

        viewLayout.text_time?.text = "Berlaku sampai $expired"
        viewLayout.text_point?.text = formatPoint(point)
        viewLayout.text_title?.text = dataVoucher?.judul
        viewLayout.text_code?.text = dataVoucher?.kode
        viewLayout.text_ketentuan?.text = dataVoucher?.ketentuan
        viewLayout.text_cara_pakai?.text = dataVoucher?.cara_pakai

        viewLayout.btn_close?.setOnClickListener { hiddenLayout() }
        viewLayout.text_copy?.setOnClickListener {
            context?.let { it1 -> copyToClipboard(it1, dataVoucher?.kode, "Kode Kupon") }
        }
        viewLayout.button_buy?.setOnClickListener {
            presenter?.redeemVoucher(dataVoucher?.id.toString())
        }

    }

    override fun showLoading(key: String, status: Boolean) {
        super.showLoading(key, status)
        activity?.runOnUiThread {
            when (key) {
                presenter?.redeemVoucherParam -> {
                    if (status) {
                        TransitionManager.beginDelayedTransition(viewLayout.parent_button)
                        viewLayout.loading?.visible()
                        viewLayout.button_buy?.gone()
                    } else {
                        GlobalScope.launch {
                            delay(1300)

                            TransitionManager.beginDelayedTransition(viewLayout.parent_button)
                            viewLayout.loading?.gone(activity)
                            viewLayout.button_buy?.visible(activity)
                            viewLayout.button_buy?.disable(activity)
                        }
                    }
                }
            }
        }
    }

    override fun requestSuccess(key: String, response: Any?) {
        super.requestSuccess(key, response)
        activity?.runOnUiThread {
            when (key) {
                presenter?.redeemVoucherParam -> {
                    val body = JSONObject(response.toString())
                    val message = if (body.has("message")) {
                        body.getString("message")
                    } else {
                        ""
                    }
                    val data = if (body.has("data")) {
                        body.getString("data")
                    } else {
                        ""
                    }

                    if (data != "") {
                        actionReedem(true)
                        return@runOnUiThread
                    }

                    actionReedem(false, message)
                }
            }
        }
    }

    override fun requestFailure(key: String, code: Int, message: Any?) {
        super.requestFailure(key, code, message)
        activity?.runOnUiThread {
            when (key) {
                presenter?.redeemVoucherParam -> {
                    actionReedem(false, message.toString())
                }
            }
        }
    }

    override fun initView() {}

    private fun hiddenLayout() {
        (behavior as BottomSheetBehavior<*>).state = BottomSheetBehavior.STATE_HIDDEN
    }

    private fun actionReedem(isSuccess: Boolean, message: String = "") {
        GlobalScope.launch {
            delay(1500)

            if (isSuccess) reedemSuccess(point) else {
                reedemFailure(message)
            }

            hiddenLayout()
        }
    }

    private fun reedemSuccess(point: Int) {
        listenerReedem?.onReedemSuccess(point)
    }

    private fun reedemFailure(message: String) {
        listenerReedem?.onReedemFailure(message)
    }

}
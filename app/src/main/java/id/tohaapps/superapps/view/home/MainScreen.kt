package id.tohaapps.superapps.view.home

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import id.tohaapps.superapps.R
import id.tohaapps.superapps.view.home.item.akun.AkunFragment
import id.tohaapps.superapps.view.home.item.event.EventFragment
import id.tohaapps.superapps.view.home.item.explore.ExploreFragment
import id.tohaapps.superapps.view.home.item.home.HomeFragment
import id.tohaapps.superapps.view.home.item.shop.ShopFragment

enum class MainScreen(@IdRes val menuItemId: Int, val fragment: Fragment) {
    HOME(R.id.menu_home, HomeFragment()),
    EXPLORE(R.id.menu_explore, ExploreFragment()),
    SHOP(R.id.menu_shop, ShopFragment()),
    EVENT(R.id.menu_event, EventFragment()),
    AKUN(R.id.menu_akun, AkunFragment())
}

fun getMainScreenForMenuItem(menuItemId: Int): MainScreen? {
    for (mainScreen in MainScreen.values()) {
        if (mainScreen.menuItemId == menuItemId) {
            return mainScreen
        }
    }
    return null
}
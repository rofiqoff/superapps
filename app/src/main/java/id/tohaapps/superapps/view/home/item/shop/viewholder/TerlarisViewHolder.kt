package id.tohaapps.superapps.view.home.item.shop.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import id.tohaapps.superapps.support.baseUrl
import id.tohaapps.superapps.support.displayImage
import id.tohaapps.superapps.view.home.item.shop.model.ProductModel
import kotlinx.android.synthetic.main.row_content_shop_terlaris.view.*

class TerlarisViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun onBind(model: ProductModel) {
        val image = model.img
        val imageUrl = if (image?.startsWith("/") == true) {
            val a = image.removeRange(0, 0)
            "$baseUrl$a"
        } else {
            image
        }

        itemView.image_content?.displayImage(itemView.context, imageUrl)
    }
}
package id.tohaapps.superapps.view.home.item.shop.viewholder

import android.app.Activity
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import id.tohaapps.superapps.R
import id.tohaapps.superapps.support.openWebPage
import id.tohaapps.superapps.view.home.item.shop.model.StoreModel
import kotlinx.android.synthetic.main.row_content_shop_store.view.*

class StoreViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun onBind(activity: Activity?, model: StoreModel) {

        itemView.text_title?.text = model.nama

        val name = model.nama.toString()

        val textBackground = when {
            name.contains("Tokopedia", true) -> R.drawable.shape_gradient_green
            name.contains("Shopee", true) -> R.drawable.shape_gradient_orange
            name.contains("Blibli.com", true) -> R.drawable.shape_gradient_blue
            name.contains("Lazada", true) -> R.drawable.shape_gradient_yellow
            name.contains("Bukalapak", true) -> R.drawable.shape_gradient_pink
            name.contains("Elevenia", true) -> R.drawable.shape_gradient_elevenia
            else -> R.drawable.shape_gradient_blue
        }

        itemView.text_title?.setBackgroundResource(textBackground)
        itemView.rootView?.setOnClickListener { activity?.openWebPage(model.url) }

    }
}
package id.tohaapps.superapps.view.coupon.module

interface IVoucherIteractor {
    fun getAllVoucher(auth: String): Pair<Int, String?>
    fun getMyVoucher(auth: String): Pair<Int, String?>
    fun redeemVoucher(auth: String, id: String): Pair<Int, String?>
}

interface IVoucherPresenter {
    fun getAllVoucher()
    fun getMyVoucher()
    fun redeemVoucher(id: String)
}
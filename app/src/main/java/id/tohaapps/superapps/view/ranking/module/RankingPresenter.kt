package id.tohaapps.superapps.view.ranking.module

import android.content.Context
import id.tohaapps.superapps.repository.AppApiClient
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.repository.data.AppSession
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class RankingPresenter(var context: Context, var view: ViewNetworkState) : IRankingPresenter {

    private val iteractor by lazy { RankingIteractor(AppApiClient.mainClient()) }

    val listRankingParam = "list.ranking.param"
    val rankingParam = "ranking.param"

    override fun getListRanking(page: Int) {
        view.networkState = NetworkingState.ShowLoading(Pair(listRankingParam, true))
        GlobalScope.launch {
            val response =
                iteractor.getListRanking(AppSession(context).profile?.token.toString(), page)

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(listRankingParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(listRankingParam, response.second))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            listRankingParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }

    override fun getRanking() {
        view.networkState = NetworkingState.ShowLoading(Pair(rankingParam, true))
        GlobalScope.launch {
            val response = iteractor.getRanking(AppSession(context).profile?.token.toString())

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(rankingParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(rankingParam, response.second))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            rankingParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }
}
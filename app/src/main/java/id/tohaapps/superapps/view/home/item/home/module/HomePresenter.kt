package id.tohaapps.superapps.view.home.item.home.module

import android.content.Context
import id.tohaapps.superapps.repository.AppApiClient
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.repository.data.AppSession
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class IklanPresenter(var context: Context, var view: ViewNetworkState) : IIklanPresenter {

    private val iteractor by lazy { IklanIteractor(AppApiClient.mainClient()) }

    val getIklanParam = "get.iklan.param"

    override fun getIklan() {
        view.networkState = NetworkingState.ShowLoading(Pair(getIklanParam, true))
        GlobalScope.launch {
            val response = iteractor.getIklan(AppSession(context).profile?.token.toString())

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(getIklanParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(getIklanParam, response.second))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            getIklanParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }
}

class BannerPresenter(var context: Context, var view: ViewNetworkState) : IBannerPresenter {
    private val iteractor by lazy { BannerIteractor(AppApiClient.mainClient()) }

    val getBannerParam = "get.banner.param"

    override fun getBanner() {
        view.networkState = NetworkingState.ShowLoading(Pair(getBannerParam, true))
        GlobalScope.launch {
            val response = iteractor.getBanner(AppSession(context).profile?.token.toString())

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(getBannerParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(getBannerParam, response.second))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            getBannerParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }

}
package id.tohaapps.superapps.view.coupon.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import id.tohaapps.superapps.support.*
import id.tohaapps.superapps.view.coupon.listener.VoucherClickListener
import id.tohaapps.superapps.view.coupon.model.VoucherModel
import kotlinx.android.synthetic.main.row_content_coupon.view.*

class CouponViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun onBind(model: VoucherModel, listener: VoucherClickListener) {
        val image = model.ikon
        val tittle = model.judul
        val expired = model.tanggal_berlaku
        val point = model.poin_minimal.toString().toInt()

        val imageUrl = if (image?.startsWith("/") == true) {
            val a = image.removeRange(0, 0)
            "$baseUrl$a"
        } else {
            image
        }

        itemView.image_coupon?.displayImage(itemView.context, imageUrl)

        val time = parseTimeUTC(expired, "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy")

        itemView.text_title?.text = tittle
        itemView.text_time?.text = "Berlaku sampai $time"
        itemView.text_point?.text = formatPoint(point)

        itemView.rootView?.setOnClickListener { listener.onClickVoucher(model) }

    }

}
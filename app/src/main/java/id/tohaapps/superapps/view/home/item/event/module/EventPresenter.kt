package id.tohaapps.superapps.view.home.item.event.module

import android.content.Context
import id.tohaapps.superapps.repository.AppApiClient
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.repository.data.AppSession
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class EventPresenter(var context: Context, var view: ViewNetworkState) : IEventPresenter {

    private val iteractor by lazy { EventIteractor(AppApiClient.mainClient()) }

    val eventParam = "event.param"

    override fun getEvent() {
        view.networkState = NetworkingState.ShowLoading(Pair(eventParam, true))
        GlobalScope.launch {
            val response = iteractor.getEvent(AppSession(context).profile?.token.toString())

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(eventParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(eventParam, response.second))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            eventParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }

}
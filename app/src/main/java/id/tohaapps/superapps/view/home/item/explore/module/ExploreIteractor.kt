package id.tohaapps.superapps.view.home.item.explore.module

import id.tohaapps.superapps.repository.api.MainApi
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.support.ERROR_MESSAGE
import id.tohaapps.superapps.support.FAILED_LOGOUT
import id.tohaapps.superapps.support.FAILED_MESSAGE
import id.tohaapps.superapps.support.showLog
import okhttp3.RequestBody

class ExploreIteractor(var api: MainApi) : IExploreIteractor {
    override fun createStatus(auth: String, body: RequestBody): Pair<Int, String?> {
        return try {
            val response = api.createStatus("Bearer $auth", body).execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    val code = response.code()

                    val message = if (code == 401) FAILED_LOGOUT else ERROR_MESSAGE

                    Pair(code, message)
                }
            }

        } catch (e: Exception) {
            showLog("failed create status : ${e.message}")
            Pair(ResponseCode.TIME_OUT, FAILED_MESSAGE)
        }
    }

    override fun getStatus(auth: String): Pair<Int, String?> {
        return try {
            val response = api.getAllStatus("Bearer $auth").execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    val code = response.code()

                    val message = if (code == 401) FAILED_LOGOUT else ERROR_MESSAGE

                    Pair(code, message)
                }
            }

        } catch (e: Exception) {
            showLog("failed get all status : ${e.message}")
            Pair(ResponseCode.TIME_OUT, FAILED_MESSAGE)
        }
    }

}
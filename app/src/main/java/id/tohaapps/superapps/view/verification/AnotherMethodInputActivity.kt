package id.tohaapps.superapps.view.verification

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.tohaapps.superapps.R
import id.tohaapps.superapps.support.isValidEmail
import id.tohaapps.superapps.support.showActivity
import id.tohaapps.superapps.support.showToast
import kotlinx.android.synthetic.main.activity_another_method_input.*

class AnotherMethodInputActivity : AppCompatActivity() {

    private var inputType: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_another_method_input)
        initView()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    private fun initView() {
        inputType = intent?.getStringExtra(VerificationActivity.PARAM_TYPE)

        initToolbar()

        var textLabel = getString(R.string.text_email)
        var hintInput = getString(R.string.text_hint_input_email)
        var textMessage = getString(R.string.message_re_input_email)

        when (inputType) {
            VerificationActivity.PARAM_TYPE_EMAIL -> {
                textLabel = getString(R.string.text_phone_number)
                hintInput = getString(R.string.text_hint_input_phone_number)
                textMessage = getString(R.string.message_re_input_phone)
            }
        }

        text_label?.text = textLabel
        edit_input?.hint = hintInput
        text_message?.text = textMessage

        button_send?.setOnClickListener {
            val inputText = edit_input?.text.toString().trim()

            if (inputText.isEmpty()) {
                showToast(getString(R.string.message_empty_input_login))
                return@setOnClickListener
            }

            when (inputType) {
                VerificationActivity.PARAM_TYPE_PHONE -> {
                    val typeInput = if (isValidEmail(inputText)) {
                        VerificationActivity.PARAM_TYPE_EMAIL
                    } else {
                        showToast(getString(R.string.message_toast_error_re_input_email))
                        return@setOnClickListener
                    }

                    showActivity(
                        VerificationActivity::class.java,
                        VerificationActivity.PARAM_TYPE, typeInput,
                        VerificationActivity.PARAM_TEXT, inputText, true
                    )
                }

                VerificationActivity.PARAM_TYPE_EMAIL -> {
                    val typeInput = if (inputText.startsWith("0")) {
                        if (inputText.length > 8) {
                            VerificationActivity.PARAM_TYPE_PHONE
                        } else {
                            showToast(getString(R.string.message_toast_error_re_input_phone))
                            return@setOnClickListener
                        }
                    } else if (inputText.startsWith("+62")) {
                        if (inputText.length > 10) {
                            VerificationActivity.PARAM_TYPE_PHONE
                        } else {
                            showToast(getString(R.string.message_toast_error_re_input_phone))
                            return@setOnClickListener
                        }
                    } else {
                        showToast(getString(R.string.message_toast_error_re_input_phone))
                        return@setOnClickListener
                    }

                    showActivity(
                        VerificationActivity::class.java,
                        VerificationActivity.PARAM_TYPE, typeInput,
                        VerificationActivity.PARAM_TEXT, inputText, true
                    )

                }
            }

        }

    }

    private fun initToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val title = when (inputType) {
            VerificationActivity.PARAM_TYPE_PHONE -> getString(R.string.label_verification_email)
            VerificationActivity.PARAM_TYPE_EMAIL -> getString(R.string.label_verification)
            else -> getString(R.string.label_verification)
        }

        supportActionBar?.title = title
    }
}

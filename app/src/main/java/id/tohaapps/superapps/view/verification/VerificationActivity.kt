package id.tohaapps.superapps.view.verification

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import id.tohaapps.superapps.R
import id.tohaapps.superapps.support.*
import id.tohaapps.superapps.repository.data.AppSession
import id.tohaapps.superapps.view.home.main.HomeActivity
import kotlinx.android.synthetic.main.activity_verification.*

class VerificationActivity : AppCompatActivity() {

    companion object {
        const val PARAM_TYPE = "param.type"
        const val PARAM_TEXT = "param.text"
        const val PARAM_IS_REGISTER = "param.isregister"

        const val PARAM_TYPE_EMAIL = "param.type.email"
        const val PARAM_TYPE_PHONE = "param.type.phone"
        const val PARAM_IS_REGISTER_TRUE = "param.register.true"
    }

    private var inputType: String? = null
    private var inputText: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification)
        initView()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    private fun initView() {
        inputType = intent?.getStringExtra(PARAM_TYPE)
        inputText = intent?.getStringExtra(PARAM_TEXT)
        val isRegister = intent?.getStringExtra(PARAM_IS_REGISTER)

        if (isRegister != null) {
            text_atau?.gone()
            text_use_another?.gone()
        }

        initToolbar()

        var messageTitle = ""

        when (inputType) {
            PARAM_TYPE_EMAIL -> {
                messageTitle = "Kode verifikasi telah dikirimkan melalui Email ke $inputText"
            }
            PARAM_TYPE_PHONE -> {
                messageTitle = "Kode verifikasi telah dikirimkan melalui SMS ke $inputText"
            }
        }

        message_title?.text = messageTitle

        text_otp?.setAnimationEnable(true)
        text_otp?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                changeLineColorOtp(R.color.colorGrey)
                message_error?.invisible()

                if (p0?.length.toString().toInt() >= 4) {
                    changeLineColorOtp(R.color.colorGreen)
                    button_verification?.enable()
                } else {
                    button_verification?.disable()
                }
            }

        })

        text_resend?.setOnClickListener {
            text_otp?.setText("")
            showToast("Kode berhasil di kirim")
        }

        text_use_another?.setOnClickListener {
            showActivity(
                AnotherMethodInputActivity::class.java,
                PARAM_TYPE, inputType.toString(), true
            )
        }

        button_verification?.setOnClickListener {
            val textOtp = text_otp?.text.toString()
            if (textOtp != "1234") {
                changeLineColorOtp(R.color.colorRed)
                message_error?.visible()
            } else {
                showActivityWithClearTop(HomeActivity::class.java)
                AppSession(baseContext).isLogin(true)
            }
        }

    }

    private fun initToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val title = when (inputType) {
            PARAM_TYPE_PHONE -> getString(R.string.label_verification)
            PARAM_TYPE_EMAIL -> getString(R.string.label_verification_email)
            else -> getString(R.string.label_verification)
        }

        supportActionBar?.title = title
    }

    private fun changeLineColorOtp(color: Int) {
        text_otp?.setLineColor(ResourcesCompat.getColor(resources, color, theme))
    }
}

package id.tohaapps.superapps.view.home.item.event.module

import id.tohaapps.superapps.repository.api.MainApi
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.support.ERROR_MESSAGE
import id.tohaapps.superapps.support.FAILED_LOGOUT
import id.tohaapps.superapps.support.FAILED_MESSAGE
import id.tohaapps.superapps.support.showLog

class EventIteractor(var api: MainApi) : IEventIteractor {
    override fun getEvent(auth: String): Pair<Int, String?> {
        return try {
            val response = api.getEvent("Bearer $auth").execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    val message = when (response.code()) {
                        401 -> FAILED_LOGOUT
                        else -> ERROR_MESSAGE
                    }

                    Pair(response.code(), message)
                }
            }

        } catch (e: Exception) {
            showLog("failed get event : ${e.message}")
            Pair(ResponseCode.TIME_OUT, FAILED_MESSAGE)
        }
    }

}
package id.tohaapps.superapps.view.home.item.home.viewholder

import android.app.Activity
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import id.tohaapps.superapps.support.displayImage
import id.tohaapps.superapps.support.formatPoint
import id.tohaapps.superapps.support.gone
import id.tohaapps.superapps.view.home.item.home.listener.HomeListener
import id.tohaapps.superapps.view.home.item.home.model.IklanModel
import kotlinx.android.synthetic.main.row_content_promo.view.*

class PromoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun onBind(context: Activity?, model: IklanModel, listener: HomeListener) {

        val points = model.poin ?: "0"

        itemView.image_banner?.displayImage(itemView.context, model.img)

        itemView.text_count_play?.gone()
        itemView.text_time?.gone()
        itemView.text_title?.text = model.judul
        itemView.text_message?.text = model.deskripsi
        itemView.text_point?.text = "+${formatPoint(points.toInt())}"

        val isShared = model.shared

        itemView.button_share?.setOnClickListener {
            listener.onShareItem(isShared, model.id, model.judul.toString(), model.url)
        }

        itemView.root?.setOnClickListener {
            listener.onItemListClick(model)
        }
    }
}
package id.tohaapps.superapps.view.home.item.home.listener

import id.tohaapps.superapps.view.home.item.home.model.IklanModel

interface HomeListener {
    fun onItemListClick(data: IklanModel)
    fun onShareItem(isShared: Boolean?, iklanId: String?, memo: String, url: String?)
    fun onAddPoint(iklanId: String, memo: String)
}
package id.tohaapps.superapps.view.ranking

import android.os.Bundle
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import id.tohaapps.superapps.R
import id.tohaapps.superapps.repository.IView
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.base.BaseActivity
import id.tohaapps.superapps.support.*
import id.tohaapps.superapps.support.adapter.Adapter
import id.tohaapps.superapps.view.ranking.model.ItemRangkingModel
import id.tohaapps.superapps.view.ranking.model.ListRankingModel
import id.tohaapps.superapps.view.ranking.module.RankingPresenter
import id.tohaapps.superapps.view.ranking.viewholder.RankingAllViewHolder
import id.tohaapps.superapps.view.ranking.viewholder.RankingTopViewHolder
import kotlinx.android.synthetic.main.activity_ranking.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class RankingActivity : BaseActivity(), IView,
    ViewNetworkState {

    private val presenter by lazy { RankingPresenter(baseContext, this) }
    private var isRefresh = false

    lateinit var adapterDataAll: Adapter<ItemRangkingModel, RankingAllViewHolder>

    private var page = 1
    private var isLastPages = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ranking)
        initView()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onDestroy() {
        super.onDestroy()
        networkState = NetworkingState.Destroy()
    }

    override fun showLoading(key: String, status: Boolean) {
        super.showLoading(key, status)
        runOnUiThread {
            if (isRefresh) {
                swipe_refresh?.apply { if (status) show() else hide() }
            } else {
                if (status) {
                    swipe_refresh?.disable()
                    loading?.visible()
                    parent_list?.gone()
                } else {
                    swipe_refresh?.enable()
                    loading?.gone()
                    parent_list?.visible()
                }
            }
        }
    }

    override fun requestSuccess(key: String, response: Any?) {
        super.requestSuccess(key, response)
        runOnUiThread {
            val respons = response.toString()

            val model = stringToObject(respons, ListRankingModel::class.java)

            val sendiri = model.self
            val listData = model.data
//                            page = model.current_page.toString().toInt()
            val lastPage = model.last_page.toString().toInt()

            val dataTop = ArrayList<ItemRangkingModel>()
            val dataAll = ArrayList<ItemRangkingModel>()

            listData?.forEachIndexed { position, data ->
                if (page == 1) {
                    if (position <= 2) {
                        dataTop.add(data)
                    } else {
                        dataAll.add(data)
                    }
                } else {
                    dataAll.add(data)
                }
            }

            if (page == 1) {
                initListRangkingTop(dataTop, sendiri?.no)

                if (listData?.size.toString().toInt() > 3) {
                    initListRangkingAll(dataAll, sendiri?.no)
                } else {
                    parent_list_all?.gone()
                }

            } else {
                adapterDataAll.addListData(dataAll)
            }

//                            if (listData?.size.toString().toInt() > 3) {
//                                if (page == 1) {
//                                    initListRangkingAll(dataAll, sendiri?.no)
//                                } else {
//                                    adapterDataAll.addListData(dataAll)
//                                }
//                            } else {
//                                parent_list_all?.gone()
//                            }

            if (page < lastPage) page++ else isLastPages = true

            val myRanking = sendiri?.no.toString().toInt()
            val myName = sendiri?.name.toString()
            val myPoint = sendiri?.poin.toString()

            if (myRanking > 10) {
                root_my_rangking?.visible()
                line_gradient?.visible()

                text_my_number?.text = myRanking.toString()
                text_my_name?.text = myName
                text_my_point?.text = formatPoint(myPoint)
            } else {
                root_my_rangking?.gone()
                line_gradient?.gone()
            }

        }
    }

    override fun requestFailure(key: String, code: Int, message: Any?) {
        super.requestFailure(key, code, message)
        showToast(message.toString())
    }

    override fun initView() {
        initToolbar()
        requestRanking()

        swipe_refresh?.apply {
            setColorSchemeResources(R.color.colorGreen, R.color.colorOrange, R.color.colorRed)
            setOnRefreshListener {
                requestRanking(true, true)
            }
        }
    }

    private fun initToolbar() {
        val cal = Calendar.getInstance()
        val monYear = SimpleDateFormat("MMMM yyyy", Locale("ID")).format(cal.time)

        supportActionBar?.subtitle = "Periode $monYear"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initListRangkingTop(data: ArrayList<ItemRangkingModel>, myRanking: String?) {
        list_top_3?.apply {
            adapter = object : Adapter<ItemRangkingModel, RankingTopViewHolder>(
                R.layout.item_list_ranking_top_3,
                RankingTopViewHolder::class.java, ItemRangkingModel::class.java, data
            ) {
                override fun bindView(
                    holder: RankingTopViewHolder, model: ItemRangkingModel, position: Int
                ) {
                    holder.onBind(model, myRanking, position)
                }
            }

            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(baseContext)
        }
    }

    private fun initListRangkingAll(data: ArrayList<ItemRangkingModel>, myRanking: String?) {
        parent_list_all?.visible()

        adapterDataAll = object : Adapter<ItemRangkingModel, RankingAllViewHolder>(
            R.layout.item_list_ranking_all,
            RankingAllViewHolder::class.java, ItemRangkingModel::class.java, data
        ) {
            override fun bindView(
                holder: RankingAllViewHolder, model: ItemRangkingModel, position: Int
            ) {
                holder.onBind(model, myRanking)
            }
        }

        list_ranking_all?.apply {
            adapter = adapterDataAll

            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(baseContext)
        }

        nested_scroll?.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->

            if (v?.getChildAt(v.childCount - 1) != null) {
                if ((scrollY >= (v.getChildAt(v.childCount - 1).measuredHeight - v.measuredHeight))
                    && scrollY > oldScrollY
                ) {

                    if (!isLastPages) {
                        requestRanking(true)
                    }

                }
            }
        })

    }

//    private fun requestRanking(isRefresh: Boolean = false) {
//        this.isRefresh = isRefresh
//        presenter.getRanking()
//    }

    private fun requestRanking(isRefresh: Boolean = false, isReset: Boolean = false) {
        this.isRefresh = isRefresh
        if (isReset) {
            isLastPages = false
            page = 1
        }
        presenter.getListRanking(page)
    }

}

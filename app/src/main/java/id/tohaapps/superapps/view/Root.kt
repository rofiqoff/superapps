package id.tohaapps.superapps.view

import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import androidx.appcompat.app.AlertDialog
import id.tohaapps.superapps.R
import id.tohaapps.superapps.modulecommon.profile.ProfilePresenter
import id.tohaapps.superapps.repository.IView
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.base.BaseActivity
import id.tohaapps.superapps.repository.data.AppSession
import id.tohaapps.superapps.support.FAILED_LOGOUT
import id.tohaapps.superapps.support.showActivityWithClearTop
import id.tohaapps.superapps.support.showLog
import id.tohaapps.superapps.support.showToast
import id.tohaapps.superapps.view.home.main.HomeActivity
import id.tohaapps.superapps.view.login.LoginActivity
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class Root : BaseActivity(), ViewNetworkState, IView {

    private val presenter by lazy { ProfilePresenter(baseContext, this) }
    private val session by lazy { AppSession(baseContext) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_root)
        initView()
    }

    override fun onDestroy() {
        super.onDestroy()
        networkState = NetworkingState.Destroy()
    }

    override fun showLoading(key: String, status: Boolean) {
        super.showLoading(key, status)
    }

    override fun requestSuccess(key: String, response: Any?) {
        super.requestSuccess(key, response)
        runOnUiThread { showNextActivity() }
    }

    override fun requestFailure(key: String, code: Int, message: Any?) {
        super.requestFailure(key, code, message)
        runOnUiThread {
            when (message.toString()) {
                FAILED_LOGOUT -> {
                    if (session.isLogin()) showToast(message.toString())

                    session.clearSession()
                    showActivityWithClearTop(LoginActivity::class.java)
                }
                else -> showMessageDialog(message.toString())
            }
        }
    }

    override fun initView() {
//        val accessToken = AccessToken.getCurrentAccessToken()
//        val isLoggedIn = accessToken != null && !accessToken.isExpired
//
//        showLog("facebook accessToken : ${accessToken?.token}")
//        showLog("facebook status login : ${if (isLoggedIn) "Login" else "Belum Login"}")

        requestGetProfile()
    }

    private fun requestGetProfile() {
        presenter.getProfile()
    }

    private fun showNextActivity() {
        Handler().postDelayed({
            if (session.isLogin()) {
                showActivityWithClearTop(HomeActivity::class.java)
            } else {
                session.clearSession()
                showActivityWithClearTop(LoginActivity::class.java)
            }
        }, 1500)
    }

    private fun showMessageDialog(message: String?) {
        AlertDialog.Builder(this).apply {
            setTitle("Mohon Maaf")
            setMessage(message)
            setPositiveButton("Ulangi") { dialog, _ ->
                dialog?.dismiss()
                requestGetProfile()
            }
            setNegativeButton("Tutup") { dialog, _ ->
                dialog?.dismiss()
                finish()
            }
            setOnCancelListener {
                it.dismiss()
                finish()
            }
        }.show()
    }

    private fun printKeyHash() {
        try {
            val info =
                packageManager.getPackageInfo("id.enesis.enesmart", PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                showLog("KeyHas : ${Base64.encodeToString(md.digest(), Base64.DEFAULT)}")
            }
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }

        val sha1 = byteArrayOf(
            0x25,
            0xCE.toByte(),
            0x29.toByte(),
            0x27,
            0xB9.toByte(),
            0x27,
            0xE6.toByte(),
            0xFB.toByte(),
            0x74,
            0x20,
            0xE3.toByte(),
            0xED.toByte(),
            0xE7.toByte(),
            0x56.toByte(),
            0xD4.toByte(),
            0x83.toByte(),
            0x63,
            0x59.toByte(),
            0x94.toByte(),
            0x64.toByte()
        )
        showLog("keyhash : ${Base64.encodeToString(sha1, Base64.NO_WRAP)}")
    }

}

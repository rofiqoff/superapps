package id.tohaapps.superapps.view.home.item.event

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.tohaapps.superapps.R
import id.tohaapps.superapps.repository.IView
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.base.BaseFragment
import id.tohaapps.superapps.support.*
import id.tohaapps.superapps.view.home.item.event.adapter.EventAdapter
import id.tohaapps.superapps.view.home.item.event.adapter.RecyclerSectionItemDecoration
import id.tohaapps.superapps.view.home.item.event.listener.ScheduleEventListner
import id.tohaapps.superapps.view.home.item.event.model.EventModel
import id.tohaapps.superapps.view.home.item.event.module.EventPresenter
import kotlinx.android.synthetic.main.fragment_event.*

class EventFragment : BaseFragment(), ScheduleEventListner, ViewNetworkState, IView {

    private val presenter by lazy { context?.let { EventPresenter(it, this) } }

    private var selectedPosition = 0
    private var isRefresh = false

    private var sectionItemDecoration: RecyclerSectionItemDecoration? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this item
        return inflater.inflate(R.layout.fragment_event, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onSelectSchedule(position: Int, value: String) {
        selectedPosition = position
        text_choose_date?.text = "Event $value"

        when (selectedPosition) {
            2 -> showLayoutEmpty()
            else -> showLayoutEmpty(false)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        networkState = NetworkingState.Destroy()
    }

    override fun showLoading(key: String, status: Boolean) {
        super.showLoading(key, status)
        activity?.runOnUiThread {
            when (key) {
                presenter?.eventParam -> {
                    if (isRefresh) {
                        swipe_refresh?.apply { if (status) show() else hide() }
                    } else {
                        if (status) {
                            swipe_refresh?.disable()
                            loading?.visible()
                            list?.gone()
                        } else {
                            swipe_refresh?.enable()
                            loading?.gone()
                            list?.visible()
                        }
                    }
                }
            }
        }
    }

    override fun requestSuccess(key: String, response: Any?) {
        super.requestSuccess(key, response)
        activity?.runOnUiThread {
            when (key) {
                presenter?.eventParam -> {
                    val STATUS_TYPE_Object = object : TypeToken<List<EventModel>>() {}.type
                    val listData = Gson().fromJson<List<EventModel>>(
                        response.toString(), STATUS_TYPE_Object
                    )

                    if (listData.isEmpty()) {
                        layout_empty?.visible()
                    } else {
                        initList(listData as ArrayList<EventModel>)
                    }
                }
            }
        }
    }

    override fun requestFailure(key: String, code: Int, message: Any?) {
        super.requestFailure(key, code, message)
        activity?.runOnUiThread {
            when (key) {
                presenter?.eventParam -> activity?.showToast(message.toString())
            }
        }
    }

    override fun initView() {
        requestEvent()

        text_choose_date?.text = "Event $WEEK_EVENT"

        text_choose_date?.setOnClickListener {
            BottomSheetEventSchedule.newInstance(selectedPosition, this)
                .show(childFragmentManager, "Choose Date")
        }

        swipe_refresh?.apply {
            setColorSchemeResources(R.color.colorGreen, R.color.colorOrange, R.color.colorRed)
            setOnRefreshListener { requestEvent(true) }
        }
    }

    private fun requestEvent(isRefresh: Boolean = false) {
        this.isRefresh = isRefresh

        presenter?.getEvent()
    }

    private fun initList(dataList: ArrayList<EventModel>) {
        if (isRefresh) sectionItemDecoration?.let { list?.removeItemDecoration(it) }

        val adapterEvent = EventAdapter(
            context, layoutInflater, dataList, R.layout.row_content_event
        )
        sectionItemDecoration = RecyclerSectionItemDecoration(
            resources.getDimensionPixelSize(R.dimen.recycler_section_header_height),
            true,
            adapterEvent
        )

        list?.apply {
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(sectionItemDecoration!!)
            adapter = adapterEvent
        }
    }

    private fun showLayoutEmpty(show: Boolean = true) {
        activity?.runOnUiThread {
            list?.apply { if (show) gone() else visible() }
            layout_empty?.apply { if (show) visible() else gone() }
        }
    }

}

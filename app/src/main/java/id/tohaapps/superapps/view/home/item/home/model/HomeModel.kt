package id.tohaapps.superapps.view.home.item.home.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class IklanModel(
    @SerializedName("id")var id: String?,
    @SerializedName("judul")var judul: String?,
    @SerializedName("deskripsi")var deskripsi: String?,
    @SerializedName("url")var url: String?,
    @SerializedName("img")var img: String?,
    @SerializedName("poin")var poin: String?,
    @SerializedName("sharepoin")var sharepoin: String?,
    @SerializedName("watched")var watched: Boolean?,
    @SerializedName("shared")var shared: Boolean?
) : Parcelable
package id.tohaapps.superapps.view.home.item.akun

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import id.tohaapps.superapps.BuildConfig
import id.tohaapps.superapps.R
import id.tohaapps.superapps.modulecommon.point.PointPresenter
import id.tohaapps.superapps.modulecommon.point.model.PointModel
import id.tohaapps.superapps.repository.IView
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.base.BaseFragment
import id.tohaapps.superapps.repository.data.AppSession
import id.tohaapps.superapps.support.*
import id.tohaapps.superapps.view.Root
import id.tohaapps.superapps.view.coupon.CouponActivity
import id.tohaapps.superapps.view.ranking.RankingActivity
import id.tohaapps.superapps.view.updateprofile.UpdateProfileActivity
import id.tohaapps.superapps.view.webview.WebviewActivity
import kotlinx.android.synthetic.main.fragment_akun.*

class AkunFragment : BaseFragment(), ViewNetworkState, IView {

    private val presenterPoint by lazy { context?.let { PointPresenter(it, this) } }
    private val session by lazy { context?.let { AppSession(it) } }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this item
        return inflater.inflate(R.layout.fragment_akun, container, false)
    }

    override fun onResume() {
        super.onResume()
        requestPoint()
        text_name?.text = session?.profile?.user?.name ?: "-"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onDestroy() {
        super.onDestroy()
        networkState = NetworkingState.Destroy()
    }

    override fun showLoading(key: String, status: Boolean) {
        super.showLoading(key, status)
        activity?.runOnUiThread {
            when (key) {
                presenterPoint?.getPointParam -> {
                    loading_point?.apply { if (status) visible() else gone() }
                    text_point?.apply { if (status) invisible() else visible() }
                    refresh_point?.gone()
                }
            }
        }
    }

    override fun requestSuccess(key: String, response: Any?) {
        super.requestSuccess(key, response)
        activity?.runOnUiThread {
            when (key) {
                presenterPoint?.getPointParam -> {
                    val respon = response.toString()

                    val model = stringToObject(respon, PointModel::class.java)

                    val point = model.data.poin.toString().toInt()

                    text_point?.text = formatPoint(point)
                }
            }
        }
    }

    override fun requestFailure(key: String, code: Int, message: Any?) {
        super.requestFailure(key, code, message)
        activity?.runOnUiThread {
            when (key) {
                presenterPoint?.getPointParam -> {
                    activity?.showToast(message.toString())

                    text_point?.invisible()
                    refresh_point?.visible()
                }
            }
        }
    }

    override fun initView() {
        requestPoint()

        text_app_version?.text = "App Version ${BuildConfig.VERSION_NAME}"

        refresh_point?.setOnClickListener { requestPoint() }

        text_to_my_coupon?.setOnClickListener {
            activity?.showActivity(
                CouponActivity::class.java,
                CouponActivity.EXTRA_TYPE_COUPON,
                CouponActivity.TYPE_MY_COUPON
            )
        }

        text_to_peringkat?.setOnClickListener {
            activity?.showActivity(RankingActivity::class.java)
        }

        text_to_edit_profile?.setOnClickListener {
            activity?.showActivity(UpdateProfileActivity::class.java)
        }

        text_privacy_policy?.setOnClickListener {
            activity?.showActivity(
                WebviewActivity::class.java,
                WebviewActivity.paramTitle, getString(R.string.text_privacy_policy),
                WebviewActivity.paramUrl, "https://enesis.tohaputra.com/concern/privacy_policy.html"
            )
        }

        text_term_condition?.setOnClickListener {
            activity?.showActivity(
                WebviewActivity::class.java,
                WebviewActivity.paramTitle,
                getString(R.string.text_term_condition),
                WebviewActivity.paramUrl,
                "https://enesis.tohaputra.com/concern/terms_and_conditions.html"
            )
        }

        text_beri_nilai?.setOnClickListener { activity?.showInPlayStore() }

        text_to_logout?.setOnClickListener { showConfirmLogout() }
    }

    private fun requestPoint() {
        presenterPoint?.getPoint()
    }

    private fun showConfirmLogout() {
        activity?.let {
            AlertDialog.Builder(it).apply {
                setMessage("Apakah Anda yakin ingin keluar?")
                setPositiveButton("Ya") { dialog, _ ->
                    dialog.dismiss()

                    AppSession(context).clearSession()
                    activity?.showActivityWithClearTop(Root::class.java)
                    activity?.finish()
                }
                setNegativeButton("Tidak") { dialog, _ ->
                    dialog.dismiss()
                }
            }.show()
        }
    }

}

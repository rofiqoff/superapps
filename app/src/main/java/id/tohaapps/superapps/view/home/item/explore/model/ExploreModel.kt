package id.tohaapps.superapps.view.home.item.explore.model

import com.google.gson.annotations.SerializedName

data class ExploreModel(
    var imageProfile: Int?,
    var name: String?,
    var time: String?,
    var description: String?,
    var imageContent: ArrayList<Int>?
)

data class StatusModel(
    @SerializedName("id") var id: Int?,
    @SerializedName("user") var user: User?,
    @SerializedName("status") var status: String?,
    @SerializedName("waktu") var waktu: String?,
    @SerializedName("img") var imageList: List<String>?
)

data class User(
    @SerializedName("id") var id: String?,
    @SerializedName("name") var name: String,
    @SerializedName("mobile") var mobile: String
)
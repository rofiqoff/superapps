package id.tohaapps.superapps.view.coupon

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.tohaapps.superapps.R
import id.tohaapps.superapps.repository.IView
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.base.BaseFragment
import id.tohaapps.superapps.support.*
import id.tohaapps.superapps.support.adapter.Adapter
import id.tohaapps.superapps.view.coupon.listener.MyVoucherClickListener
import id.tohaapps.superapps.view.coupon.listener.VoucherClickListener
import id.tohaapps.superapps.view.coupon.listener.VoucherReedemListener
import id.tohaapps.superapps.view.coupon.model.VoucherModel
import id.tohaapps.superapps.view.coupon.module.VoucherPresenter
import id.tohaapps.superapps.view.coupon.viewholder.CouponViewHolder
import kotlinx.android.synthetic.main.fragment_voucher.*

class AllVoucherFragment : BaseFragment(), VoucherClickListener, VoucherReedemListener,
    MyVoucherClickListener,
    IView, ViewNetworkState {

    private val presenter by lazy { context?.let { VoucherPresenter(it, this) } }
    private var isRefresh = false

    companion object {
        lateinit var listener: MyVoucherClickListener
        var currentPoint: Int? = 0

        fun newInstance(myListener: MyVoucherClickListener, myPoint: Int?): AllVoucherFragment {
            val fragment = AllVoucherFragment()
            listener = myListener
            currentPoint = myPoint
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this item
        return inflater.inflate(R.layout.fragment_voucher, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun showLoading(key: String, status: Boolean) {
        super.showLoading(key, status)
        activity?.runOnUiThread {
            when (key) {
                presenter?.allVoucherParam -> {
                    if (isRefresh) {
                        swipe_refresh?.apply { if (status) show() else hide() }
                    } else {
                        if (status) {
                            swipe_refresh?.disable()
                            loading?.visible()
                            list?.gone()
                        } else {
                            swipe_refresh?.enable()
                            loading?.gone()
                            list?.visible()
                        }
                    }
                }
            }
        }
    }

    override fun requestSuccess(key: String, response: Any?) {
        super.requestSuccess(key, response)
        activity?.runOnUiThread {
            when (key) {
                presenter?.allVoucherParam -> {
                    val statusObjectType =
                        object : TypeToken<ArrayList<VoucherModel>>() {}.type
                    val listData = Gson().fromJson<ArrayList<VoucherModel>>(
                        response.toString(), statusObjectType
                    )

                    if (listData.isEmpty()) {
                        layout_empty?.visible()
                        list?.gone()
                    } else {
                        initListVoucher(listData)
                    }
                }
            }
        }
    }

    override fun requestFailure(key: String, code: Int, message: Any?) {
        super.requestFailure(key, code, message)
        activity?.runOnUiThread {
            when (key) {
                presenter?.allVoucherParam -> {
                    activity?.showToast(message.toString())
                }
            }
        }
    }

    override fun initView() {
        requestVoucher()

        swipe_refresh?.apply {
            setColorSchemeResources(R.color.colorGreen, R.color.colorOrange, R.color.colorRed)
            setOnRefreshListener { requestVoucher(true) }
        }
    }

    override fun onClickVoucher(data: VoucherModel) {
        BottomSheetVoucherDetail.newInstance(data, false, this)
            .show(childFragmentManager, "show detail")
    }

    override fun onReedemSuccess(point: Int) {
        val a = currentPoint.toString().toInt() - point
        BottomSheetVoucherRedeemSuccess.newInstance(a, this)
            .show(childFragmentManager, "show message success")
    }

    override fun onReedemFailure(message: String?) {
        activity?.showToast(message, true)
    }

    override fun onShowMyVoucher() {
        listener.onShowMyVoucher()
    }

    private fun initListVoucher(dataList: ArrayList<VoucherModel>) {
        layout_empty?.gone()
        list?.visible()

        list?.apply {
            adapter = object : Adapter<VoucherModel, CouponViewHolder>(
                R.layout.row_content_coupon,
                CouponViewHolder::class.java, VoucherModel::class.java, dataList
            ) {
                override fun bindView(
                    holder: CouponViewHolder,
                    model: VoucherModel,
                    position: Int
                ) {
                    holder.onBind(model, this@AllVoucherFragment)
                }
            }

            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
        }

    }

    private fun requestVoucher(isRefresh: Boolean = false) {
        this.isRefresh = isRefresh

        presenter?.getAllVoucher()
    }

}

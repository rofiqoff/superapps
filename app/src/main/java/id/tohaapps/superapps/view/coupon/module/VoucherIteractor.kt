package id.tohaapps.superapps.view.coupon.module

import id.tohaapps.superapps.repository.api.MainApi
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.support.*

class VoucherIteractor(var api: MainApi) : IVoucherIteractor {

    override fun getAllVoucher(auth: String): Pair<Int, String?> {
        return try {
            val response = api.getAllVoucher("Bearer $auth").execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    val code = response.code()

                    val message = if (code == 401) FAILED_LOGOUT else FAILED_MESSAGE

                    Pair(code, message)
                }
            }

        } catch (e: Exception) {
            showLog("failed get all voucher : ${e.message}")
            Pair(ResponseCode.TIME_OUT, ERROR_MESSAGE)
        }
    }

    override fun getMyVoucher(auth: String): Pair<Int, String?> {
        return try {
            val response = api.getMyVoucher("Bearer $auth").execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    val code = response.code()

                    val message = if (code == 401) FAILED_LOGOUT else FAILED_MESSAGE

                    Pair(code, message)
                }
            }

        } catch (e: Exception) {
            showLog("failed get my voucher : ${e.message}")
            Pair(ResponseCode.TIME_OUT, ERROR_MESSAGE)
        }
    }

    override fun redeemVoucher(auth: String, id: String): Pair<Int, String?> {
        return try {
            val response = api.redeemVoucher("Bearer $auth", id).execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {

                    val message = when (response.code()) {
                        401 -> FAILED_LOGOUT
                        400 -> NEED_MORE_POINT
                        404 -> ERROR_MESSAGE
                        else -> FAILED_MESSAGE
                    }

                    Pair(response.code(), message)
                }
            }

        } catch (e: Exception) {
            showLog("failed redeem voucher : ${e.message}")
            Pair(ResponseCode.TIME_OUT, ERROR_MESSAGE)
        }
    }

}
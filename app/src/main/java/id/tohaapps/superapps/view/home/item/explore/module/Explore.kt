package id.tohaapps.superapps.view.home.item.explore.module

import okhttp3.RequestBody
import java.io.File

interface IExploreIteractor {
    fun createStatus(auth: String, body: RequestBody): Pair<Int, String?>
    fun getStatus(auth: String): Pair<Int, String?>
}

interface IExplorePresenter {
    fun createStatus(status: String, waktu: String, img: ArrayList<File>?)
    fun getStatus()
}
package id.tohaapps.superapps.view.login.model

import com.google.gson.annotations.SerializedName

data class UserModel(
    @SerializedName("user") var user: User?,
    @SerializedName("access_token") var token: String?
)

data class User(
    @SerializedName("id") var id: Int?,
    @SerializedName("name") var name: String?,
    @SerializedName("mobile") var mobile: String?,
    @SerializedName("email") var email: String?
)

data class UserNoId(var name: String?, var mobile: String?, var email: String?)
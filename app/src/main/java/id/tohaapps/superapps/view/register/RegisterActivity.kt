package id.tohaapps.superapps.view.register

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import id.tohaapps.superapps.R
import id.tohaapps.superapps.repository.IView
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.base.BaseActivity
import id.tohaapps.superapps.repository.data.AppSession
import id.tohaapps.superapps.support.*
import id.tohaapps.superapps.view.home.main.HomeActivity
import id.tohaapps.superapps.view.register.module.RegisterPresenter
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : BaseActivity(), ViewNetworkState,
    IView {

    private val loading by lazy { DialogLoading(this) }
    private val presenter by lazy { RegisterPresenter(baseContext, this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        initView()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onDestroy() {
        super.onDestroy()
        networkState = NetworkingState.Destroy()
    }

    override fun showLoading(key: String, status: Boolean) {
        super.showLoading(key, status)
        runOnUiThread {
            if (status) loading.showDialog() else loading.dismissDialog()
        }
    }

    override fun requestSuccess(key: String, response: Any?) {
        super.requestSuccess(key, response)
        runOnUiThread {
            val isHasGift = response as Boolean

            showActivityWithClearTop(HomeActivity::class.java, "isGetGift", isHasGift)
            AppSession(baseContext).isLogin(true)
        }
    }

    override fun requestFailure(key: String, code: Int, message: Any?) {
        super.requestFailure(key, code, message)
        runOnUiThread { showToast(message.toString()) }
    }

    override fun initView() {
        initToolbar()

        edit_input_fullname?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                message_error_fullname?.invisible()
            }
        })

        edit_input_email?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                message_error_email?.invisible()
            }
        })

        edit_input_phone?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                message_error_phone?.invisible()
            }
        })

        button_register?.setOnClickListener {
            val inputFullName = edit_input_fullname?.text.toString().trim()
            val inputEmail = edit_input_email?.text.toString().trim()
            val inputNumber = edit_input_phone?.text.toString().trim()

            when {
                inputFullName.isEmpty() -> {
                    edit_input_fullname?.requestFocus()
                    edit_input_fullname?.setSelection(inputFullName.length)

                    message_error_fullname?.text = getString(R.string.message_empty_input_fullname)
                    message_error_fullname?.visible()

                    return@setOnClickListener
                }
                inputNumber.isEmpty() -> {
                    edit_input_phone?.requestFocus()
                    edit_input_phone?.setSelection(inputNumber.length)

                    message_error_phone?.text = getString(R.string.message_empty_input_phone)
                    message_error_phone?.visible()

                    return@setOnClickListener
                }
                else -> {
                    val regexStr = "^[0-9]*$".toRegex()

//                    if (!isValidEmail(inputEmail)) {
//                        edit_input_email?.requestFocus()
//                        edit_input_email?.setSelection(inputEmail.length)
//
//                        message_error_email?.text = getString(R.string.message_error_format_email)
//                        message_error_email?.visible()
//
//                        return@setOnClickListener
//                    } else
                    if (inputNumber.matches(regexStr)) {
                        if (inputNumber.startsWith("0")) {
                            if (inputNumber.length < 8) {
                                edit_input_phone?.requestFocus()
                                edit_input_phone?.setSelection(inputNumber.length)

                                message_error_phone?.text =
                                    getString(R.string.message_error_format_phone)
                                message_error_phone?.visible()
                                return@setOnClickListener
                            }
                        } else {
                            edit_input_phone?.requestFocus()
                            edit_input_phone?.setSelection(inputNumber.length)

                            message_error_phone?.text =
                                getString(R.string.message_error_format_phone)
                            message_error_phone?.visible()
                            return@setOnClickListener
                        }
                    } else if (inputNumber.startsWith("+62")) {
                        if (inputNumber.length < 10) {
                            edit_input_phone?.requestFocus()
                            edit_input_phone?.setSelection(inputNumber.length)

                            message_error_phone?.text =
                                getString(R.string.message_error_format_phone)
                            message_error_phone?.visible()
                            return@setOnClickListener
                        }
                    } else {
                        edit_input_phone?.requestFocus()
                        edit_input_phone?.setSelection(inputNumber.length)

                        message_error_phone?.text = getString(R.string.message_error_format_phone)
                        message_error_phone?.visible()
                        return@setOnClickListener
                    }

                    presenter.register(inputFullName, inputNumber)

//                    showActivity(
//                        VerificationActivity::class.java,
//                        VerificationActivity.PARAM_TYPE,
//                        VerificationActivity.PARAM_TYPE_PHONE,
//                        VerificationActivity.PARAM_TEXT, inputNumber,
//                        VerificationActivity.PARAM_IS_REGISTER,
//                        VerificationActivity.PARAM_IS_REGISTER_TRUE
//                    )
                }
            }
        }

        text_login?.setOnClickListener {
            finish()
        }
    }

    private fun initToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

}

package id.tohaapps.superapps.view.coupon

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.tohaapps.superapps.R
import id.tohaapps.superapps.support.adapter.FragmentAdapter
import id.tohaapps.superapps.view.coupon.listener.MyVoucherClickListener
import id.tohaapps.superapps.view.coupon.listener.MyVoucherCountListener
import kotlinx.android.synthetic.main.activity_coupon.*

class CouponActivity : AppCompatActivity(), MyVoucherCountListener, MyVoucherClickListener {

    companion object {
        const val EXTRA_TYPE_COUPON = "extra.type"
        const val EXTRA_POINT = "extra.point"
        const val TYPE_MY_COUPON = "extra.mycoupon"
        const val TYPE_CHANGE_POINT = "extra.changepoint"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coupon)
        initView()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun countMyVoucher(count: Int) {
        var countString = "($count)"
        if (count > 99) countString = "($count+)"

        tab?.getTabAt(1)?.text = "${getString(R.string.text_kupon_saya)} $countString"
    }

    override fun onShowMyVoucher() {
        selectTab(1)
    }

    private fun initView() {
        initToolbar()

        val adapterTab = FragmentAdapter(supportFragmentManager, true)

        adapterTab.apply {
            addFragment(
                AllVoucherFragment.newInstance(
                    this@CouponActivity,
                    intent?.getIntExtra(EXTRA_POINT, 0)
                ),
                getString(R.string.text_tukar_poin)
            )
            addFragment(
                MyVoucherFragment.newInstance(this@CouponActivity),
                "${getString(R.string.text_kupon_saya)}"
            )
        }

        view_pager?.adapter = adapterTab
        tab?.setupWithViewPager(view_pager)

        when (intent?.getStringExtra(EXTRA_TYPE_COUPON)) {
            TYPE_MY_COUPON -> selectTab(1)
            TYPE_CHANGE_POINT -> selectTab(0)
        }

    }

    private fun initToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun selectTab(position: Int) {
        tab.getTabAt(position)?.select()
    }
}

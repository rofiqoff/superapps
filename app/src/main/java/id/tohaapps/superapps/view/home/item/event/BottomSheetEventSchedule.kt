package id.tohaapps.superapps.view.home.item.event

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.tohaapps.superapps.R
import id.tohaapps.superapps.support.MONTH_EVENT
import id.tohaapps.superapps.support.NEXT_MONTH_EVENT
import id.tohaapps.superapps.support.WEEK_EVENT
import id.tohaapps.superapps.support.adapter.Adapter
import id.tohaapps.superapps.view.home.item.event.listener.ScheduleEventListner
import id.tohaapps.superapps.view.home.item.event.viewholder.ScheduleEventViewHolder
import kotlinx.android.synthetic.main.row_content_schedule_event.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class BottomSheetEventSchedule : BottomSheetDialogFragment() {

    private var behavior: CoordinatorLayout.Behavior<*>? = null

    companion object {
        lateinit var listener: ScheduleEventListner
        private var selectedItem: Int = -1

        fun newInstance(position: Int, list: ScheduleEventListner): BottomSheetEventSchedule {
            val fragment = BottomSheetEventSchedule()

            selectedItem = position
            listener = list

            return fragment
        }
    }

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        BottomSheetDialog(requireContext(), theme)

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val view =
            LayoutInflater.from(context).inflate(R.layout.item_bottomsheet_event_schedule, null)
        dialog.setContentView(view)

        val params = (view?.parent as View).layoutParams as CoordinatorLayout.LayoutParams
        behavior = params.behavior

        val close = view.findViewById<AppCompatImageView>(R.id.btn_close)
        val list = view.findViewById<RecyclerView>(R.id.list)

        initList(list)

        close.setOnClickListener {
            (behavior as BottomSheetBehavior<*>).state =
                BottomSheetBehavior.STATE_HIDDEN
        }
    }

    private fun initList(list: RecyclerView) {
        val dataList = arrayListOf(WEEK_EVENT, MONTH_EVENT, NEXT_MONTH_EVENT)

        list.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = object : Adapter<String, ScheduleEventViewHolder>(
                R.layout.row_content_schedule_event,
                ScheduleEventViewHolder::class.java, String::class.java, dataList
            ) {
                override fun bindView(
                    holder: ScheduleEventViewHolder, model: String, position: Int
                ) {
                    holder.onBind(model)

                    if (selectedItem == position) {
                        val iconCheck = ContextCompat.getDrawable(context, R.drawable.ic_check)

                        holder.itemView.text_item?.setCompoundDrawablesWithIntrinsicBounds(
                            null, null, iconCheck, null
                        )
                    } else {
                        holder.itemView.text_item?.setCompoundDrawablesWithIntrinsicBounds(
                            null, null, null, null
                        )
                    }

                    holder.itemView.text_item?.setOnClickListener {
                        notifyItemChanged(position)
                        notifyItemChanged(selectedItem)
                        selectedItem = position

                        GlobalScope.launch {
                            delay(500)
                            listener.onSelectSchedule(selectedItem, model)
                            (behavior as BottomSheetBehavior<*>).state =
                                BottomSheetBehavior.STATE_HIDDEN
                        }
                    }

                }

            }
        }

    }

}
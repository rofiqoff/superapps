package id.tohaapps.superapps.view.coupon.module

import android.content.Context
import id.tohaapps.superapps.repository.AppApiClient
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.repository.data.AppSession
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class VoucherPresenter(var context: Context, var view: ViewNetworkState) : IVoucherPresenter {

    private val iteractor by lazy { VoucherIteractor(AppApiClient.mainClient()) }

    val allVoucherParam = "all.voucher.param"
    val myVoucherParam = "my.voucher.param"
    val redeemVoucherParam = "redeem.voucher.param"

    override fun getAllVoucher() {
        view.networkState = NetworkingState.ShowLoading(Pair(allVoucherParam, true))
        GlobalScope.launch {
            val response = iteractor.getAllVoucher(AppSession(context).profile?.token.toString())

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(allVoucherParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(allVoucherParam, response.second))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            allVoucherParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }

    override fun getMyVoucher() {
        view.networkState = NetworkingState.ShowLoading(Pair(myVoucherParam, true))
        GlobalScope.launch {
            val response = iteractor.getMyVoucher(AppSession(context).profile?.token.toString())

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(myVoucherParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(myVoucherParam, response.second))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            myVoucherParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }

    override fun redeemVoucher(id: String) {
        view.networkState = NetworkingState.ShowLoading(Pair(redeemVoucherParam, true))
        GlobalScope.launch {
            val response =
                iteractor.redeemVoucher(AppSession(context).profile?.token.toString(), id)

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(redeemVoucherParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        view.networkState =
                            NetworkingState.ResponseSuccess(
                                Pair(
                                    redeemVoucherParam,
                                    response.second
                                )
                            )
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            redeemVoucherParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }
}
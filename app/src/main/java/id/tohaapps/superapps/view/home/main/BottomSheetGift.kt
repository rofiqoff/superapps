package id.tohaapps.superapps.view.home.main

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.widget.AppCompatButton
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.tohaapps.superapps.R

class BottomSheetGift : BottomSheetDialogFragment() {

    private var behavior: CoordinatorLayout.Behavior<*>? = null

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        BottomSheetDialog(requireContext(), theme)

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val view =
            LayoutInflater.from(context).inflate(R.layout.item_bottomsheet_gift, null)
        dialog.setContentView(view)

        val params = (view?.parent as View).layoutParams as CoordinatorLayout.LayoutParams
        behavior = params.behavior

        val button = view.findViewById<AppCompatButton>(R.id.btn_oke)

        button.setOnClickListener {
            (behavior as BottomSheetBehavior<*>).state =
                BottomSheetBehavior.STATE_HIDDEN
        }
    }

}
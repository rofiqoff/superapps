package id.tohaapps.superapps.view.home.item.explore.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import id.tohaapps.superapps.support.*
import id.tohaapps.superapps.view.home.item.explore.model.StatusModel
import kotlinx.android.synthetic.main.row_content_explore.view.*

class ExploreViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun onBind(model: StatusModel) {
        val date = parseTimeUTC(model.waktu, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss")

        itemView.text_name?.text = model.user?.name
        itemView.text_time?.text = TimeAgo.getTimeAgo(convertDateToLong(date))
        itemView.text_description?.text = model.status

        val dataImage = model.imageList

        if (dataImage.isNullOrEmpty()) {
            itemView.card_image?.gone()
            itemView.text_to_foto_lain?.gone()
        } else {
            val image = dataImage[0]
            val imageUrl = if (image.startsWith("/")){
                val a = image.removeRange(0,0)
                "$baseUrl$a"
            } else {
                image
            }

            itemView.image_content?.displayImage(itemView.context, imageUrl)

            if (dataImage.size.toString().toInt() > 1) {
                itemView.view_shadow?.visible()
                itemView.text_to_foto_lain?.visible()
            } else {
                itemView.view_shadow?.gone()
                itemView.text_to_foto_lain?.gone()
            }
        }
    }
}
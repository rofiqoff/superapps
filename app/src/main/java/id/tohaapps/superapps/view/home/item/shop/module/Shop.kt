package id.tohaapps.superapps.view.home.item.shop.module

interface IShopIteractor {
    fun getStore(auth: String): Pair<Int, String?>
    fun getPromo(auth: String): Pair<Int, String?>
    fun getTerlaris(auth: String): Pair<Int, String?>
}

interface IShopPresenter {
    fun getStore()
    fun getPromo()
    fun getTerlaris()
}
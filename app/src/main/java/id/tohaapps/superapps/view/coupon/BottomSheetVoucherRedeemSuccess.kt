package id.tohaapps.superapps.view.coupon

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.tohaapps.superapps.R
import id.tohaapps.superapps.view.coupon.listener.MyVoucherClickListener
import kotlinx.android.synthetic.main.item_bottomsheet_voucher_redeem_succes.view.*

class BottomSheetVoucherRedeemSuccess : BottomSheetDialogFragment() {

    private var behavior: CoordinatorLayout.Behavior<*>? = null

    companion object{
        var myPoint = 0
        lateinit var myListener: MyVoucherClickListener

        fun newInstance(point: Int, listener: MyVoucherClickListener): BottomSheetVoucherRedeemSuccess{
            val fragment = BottomSheetVoucherRedeemSuccess()

            myPoint = point
            myListener = listener

            return fragment
        }
    }

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        BottomSheetDialog(requireContext(), theme)

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val view =
            LayoutInflater.from(context).inflate(R.layout.item_bottomsheet_voucher_redeem_succes, null)
        dialog.setContentView(view)

        val params = (view?.parent as View).layoutParams as CoordinatorLayout.LayoutParams
        behavior = params.behavior

        val point = myPoint

//        view.text_message_point?.text = "Sisa Poin Kamu ${formatPoint(point)}"
        view.btn_check_voucher?.setOnClickListener {
            (behavior as BottomSheetBehavior<*>).state = BottomSheetBehavior.STATE_HIDDEN
            myListener.onShowMyVoucher()
        }

    }

}
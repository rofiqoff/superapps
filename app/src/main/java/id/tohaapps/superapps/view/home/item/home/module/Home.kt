package id.tohaapps.superapps.view.home.item.home.module

interface IIklanIteractor {
    fun getIklan(auth: String): Pair<Int, String?>
}

interface IIklanPresenter {
    fun getIklan()
}

interface IBannerIteractor {
    fun getBanner(auth: String): Pair<Int, String?>
}

interface IBannerPresenter {
    fun getBanner()
}
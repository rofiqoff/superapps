package id.tohaapps.superapps.view.register.module

import android.content.Context
import id.tohaapps.superapps.repository.AppApiClient
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.repository.data.AppSession
import id.tohaapps.superapps.support.stringToObject
import id.tohaapps.superapps.view.login.model.User
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.FormBody
import org.json.JSONObject

class RegisterPresenter(var context: Context, var view: ViewNetworkState) : IRegisterPresenter {

    private val iteractor by lazy { RegisterIteractor(AppApiClient.mainClient()) }
    private val session by lazy { AppSession(context) }

    val registerParam = "register.param"

    override fun register(name: String, mobile: String) {
        view.networkState = NetworkingState.ShowLoading(Pair(registerParam, true))
        GlobalScope.launch {
            val formBody = FormBody.Builder()
                .add("name", name)
                .add("mobile", mobile)
                .build()

            val response = iteractor.register(formBody)
            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(registerParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        val body = JSONObject(response.second.toString())

                        val userString = body.getString("user")
                        val user = stringToObject(userString, User::class.java)
                        val token = body.getString("access_token")
                        val hasBenefit = body.getBoolean("has_benefit")

                        session.saveDataProfile(response.second.toString())
                        session.createSession(user)

                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(registerParam, hasBenefit))
                    }
                    else -> view.networkState =
                        NetworkingState.ResponseFailure(
                            Pair(
                                registerParam,
                                Pair(response.first, response.second)
                            )
                        )
                }

            }
        }
    }

}
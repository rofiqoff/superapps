package id.tohaapps.superapps.view.register.module

import id.tohaapps.superapps.repository.api.MainApi
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.support.*
import okhttp3.FormBody
import org.json.JSONObject

class RegisterIteractor(var api: MainApi) : IRegisterIteractor {
    override fun register(body: FormBody): Pair<Int, String?> {
        return try {
            val response = api.signUp(body).execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    val message = when (response.code()) {
                        404 -> ERROR_MESSAGE
                        else -> {
                            val errorBody = response.errorBody()?.string()
                            val json = JSONObject(errorBody)
                            val errorMessage = json.getString("message")

                            when (json.getInt("status_code")) {
                                409 -> ALREADY_EXIST_MESSAGE
                                422 -> MUST_NUMBER_MESSAGE
                                else -> errorMessage
                            }
                        }
                    }

                    Pair(response.code(), message)
                }
            }

        } catch (e: Exception) {
            showLog("failed register : ${e.message}")
            Pair(ResponseCode.TIME_OUT, FAILED_MESSAGE)
        }
    }

}
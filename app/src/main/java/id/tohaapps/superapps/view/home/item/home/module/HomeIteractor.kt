package id.tohaapps.superapps.view.home.item.home.module

import id.tohaapps.superapps.repository.api.MainApi
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.support.ERROR_MESSAGE
import id.tohaapps.superapps.support.FAILED_LOGOUT
import id.tohaapps.superapps.support.FAILED_MESSAGE
import id.tohaapps.superapps.support.showLog

class IklanIteractor(var api: MainApi) : IIklanIteractor {
    override fun getIklan(auth: String): Pair<Int, String?> {
        return try {
            val response = api.getIklan("Bearer $auth").execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    val message = when (response.code()) {
                        401 -> FAILED_LOGOUT
                        else -> ERROR_MESSAGE
                    }

                    Pair(response.code(), message)
                }
            }

        } catch (e: Exception) {
            showLog("failed get iklan : ${e.message}")
            Pair(ResponseCode.TIME_OUT, FAILED_MESSAGE)
        }
    }
}

class BannerIteractor(var api: MainApi) : IBannerIteractor {
    override fun getBanner(auth: String): Pair<Int, String?> {
        return try {
            val response = api.getBanner("Bearer $auth").execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    val message = when (response.code()) {
                        401 -> FAILED_LOGOUT
                        else -> ERROR_MESSAGE
                    }

                    Pair(response.code(), message)
                }
            }

        } catch (e: Exception) {
            showLog("failed get iklan : ${e.message}")
            Pair(ResponseCode.TIME_OUT, FAILED_MESSAGE)
        }
    }
}
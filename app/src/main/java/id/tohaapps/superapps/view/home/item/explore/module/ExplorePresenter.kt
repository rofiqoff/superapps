package id.tohaapps.superapps.view.home.item.explore.module

import android.content.Context
import id.tohaapps.superapps.repository.AppApiClient
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.repository.data.AppSession
import id.tohaapps.superapps.support.showLog
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class ExplorePresenter(var context: Context, var view: ViewNetworkState) : IExplorePresenter {

    private val iteractor by lazy { ExploreIteractor(AppApiClient.mainClient()) }

    val createStatusParam = "create.status.param"
    val getStatusParam = "get.status.param"

    override fun createStatus(status: String, waktu: String, img: ArrayList<File>?) {
        showLog("requestCreateStatus: $status, $waktu, ${img?.size}")

        val body = MultipartBody.Builder().setType(MultipartBody.FORM)

        body.addFormDataPart("status", status)
        body.addFormDataPart("waktu", waktu)

        img?.forEach {
            body.addFormDataPart(
                "img[]", it.name,
                RequestBody.create("image/*".toMediaTypeOrNull(), it)
            )
        }

        view.networkState = NetworkingState.ShowLoading(Pair(createStatusParam, true))

        GlobalScope.launch {
            val response =
                iteractor.createStatus(AppSession(context).profile?.token.toString(), body.build())

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(createStatusParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        view.networkState =
                            NetworkingState.ResponseSuccess(
                                Pair(
                                    createStatusParam,
                                    response.second
                                )
                            )
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            createStatusParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }

    override fun getStatus() {
        view.networkState = NetworkingState.ShowLoading(Pair(getStatusParam, true))
        GlobalScope.launch {
            val response = iteractor.getStatus(AppSession(context).profile?.token.toString())

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(getStatusParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(getStatusParam, response.second))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            getStatusParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }

}
package id.tohaapps.superapps.view.updateprofile

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import id.tohaapps.superapps.R
import id.tohaapps.superapps.repository.IView
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.base.BaseActivity
import id.tohaapps.superapps.repository.data.AppSession
import id.tohaapps.superapps.support.DialogLoading
import id.tohaapps.superapps.support.disable
import id.tohaapps.superapps.support.enable
import id.tohaapps.superapps.support.showToast
import id.tohaapps.superapps.view.login.model.UserNoId
import id.tohaapps.superapps.view.updateprofile.module.UpdateProfilePresenter
import kotlinx.android.synthetic.main.activity_update_profile.*

class UpdateProfileActivity : BaseActivity(), ViewNetworkState, IView {

    private val presenter by lazy { UpdateProfilePresenter(baseContext, this) }
    private val loading by lazy { DialogLoading(this) }
    private val session by lazy { AppSession(baseContext) }

    private var nameUpdate = ""
    private var mobileUpdate = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_profile)
        initView()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onDestroy() {
        super.onDestroy()
        networkState = NetworkingState.Destroy()
    }

    override fun showLoading(key: String, status: Boolean) {
        super.showLoading(key, status)
        runOnUiThread {
            if (status) loading.showDialog() else loading.dismissDialog()
        }
    }

    override fun requestSuccess(key: String, response: Any?) {
        super.requestSuccess(key, response)
        runOnUiThread {
            val user = UserNoId(nameUpdate, mobileUpdate, session.profile?.user?.email)

            session.updateSession(user)

            onBackPressed()
        }
    }

    override fun requestFailure(key: String, code: Int, message: Any?) {
        super.requestFailure(key, code, message)
        runOnUiThread { showToast(message.toString()) }
    }

    override fun initView() {
        initToolbar()

        val name = session.profile?.user?.name ?: "-"
        val mobile = session.profile?.user?.mobile ?: "-"

        edit_input_fullname?.setText(name)
        edit_input_phone?.setText(mobile)

        if ((edit_input_fullname?.text?.isNotEmpty() == true)
            && edit_input_phone?.text?.isNotEmpty() == true
        ) {
            button_save?.enable()
        }

        edit_input_fullname?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if ((p0?.length.toString().toInt() > 0)
                    && (edit_input_phone?.text?.isNotEmpty() == true)
                ) {
                    button_save?.enable()
                } else {
                    button_save?.disable()
                }
            }
        })

        edit_input_phone?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if ((p0?.length.toString().toInt() > 0)
                    && (edit_input_fullname?.text?.isNotEmpty() == true)
                ) {
                    button_save?.enable()
                } else {
                    button_save?.disable()
                }
            }
        })

        button_save?.setOnClickListener {
            nameUpdate = edit_input_fullname?.text.toString().trim()
            mobileUpdate = edit_input_phone?.text.toString().trim()

            requestUpdate(nameUpdate, mobileUpdate)
        }
    }

    private fun initToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun requestUpdate(name: String, mobile: String) {
        presenter.updateProfile(name, mobile)
    }

}

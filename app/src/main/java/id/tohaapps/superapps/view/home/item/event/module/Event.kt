package id.tohaapps.superapps.view.home.item.event.module

interface IEventIteractor {
    fun getEvent(auth: String): Pair<Int, String?>
}

interface IEventPresenter {
    fun getEvent()
}
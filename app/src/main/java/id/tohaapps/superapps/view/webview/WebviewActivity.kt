package id.tohaapps.superapps.view.webview

import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import id.tohaapps.superapps.R
import id.tohaapps.superapps.support.gone
import id.tohaapps.superapps.support.visible
import kotlinx.android.synthetic.main.activity_webview.*

class WebviewActivity : AppCompatActivity() {

    companion object {
        val paramTitle = "param.title"
        val paramUrl = "param.url"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)

        val title = intent?.getStringExtra(paramTitle)
        val url = intent?.getStringExtra(paramUrl)

        supportActionBar?.title = title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initWebview(url)

    }

    override fun onBackPressed() {
        if (web_view?.canGoBack() == true) {
            web_view?.goBack()
        } else {
            super.onBackPressed()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    private fun initWebview(url: String?) {
        web_view?.settings?.javaScriptEnabled = true
        web_view?.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                val urlS = request?.url.toString()
                view?.loadUrl(urlS)
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                loading?.progress = 100
                loading?.gone()
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                loading?.progress = 0
                loading?.visible()
            }
        }
        web_view?.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                loading?.progress = newProgress
            }
        }

        web_view?.loadUrl(url)
    }

}

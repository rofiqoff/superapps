package id.tohaapps.superapps.modulecommon.point

import id.tohaapps.superapps.repository.api.MainApi
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.support.ERROR_MESSAGE
import id.tohaapps.superapps.support.FAILED_LOGOUT
import id.tohaapps.superapps.support.FAILED_MESSAGE
import id.tohaapps.superapps.support.showLog
import okhttp3.FormBody

class PointIteractor(var api: MainApi) : IPointIteractor {

    override fun addPointShare(auth: String, body: FormBody): Pair<Int, String?> {
        return try {
            val response = api.addPointShare("Bearer $auth", body).execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    val message = when (response.code()) {
                        401 -> FAILED_LOGOUT
                        422 -> ""
                        else -> ERROR_MESSAGE
                    }

                    Pair(response.code(), message)
                }
            }

        } catch (e: Exception) {
            showLog("failed add point share : ${e.message}")
            Pair(ResponseCode.TIME_OUT, FAILED_MESSAGE)
        }
    }

    override fun addPointWatch(auth: String, body: FormBody): Pair<Int, String?> {
        return try {
            val response = api.addPointWatch("Bearer $auth", body).execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    val message = when (response.code()) {
                        401 -> FAILED_LOGOUT
                        422 -> ""
                        else -> ERROR_MESSAGE
                    }

                    Pair(response.code(), message)
                }
            }

        } catch (e: Exception) {
            showLog("failed add point watch : ${e.message}")
            Pair(ResponseCode.TIME_OUT, FAILED_MESSAGE)
        }
    }

    override fun addPoint(auth: String, body: FormBody): Pair<Int, String?> {
        return try {
            val response = api.addPoint("Bearer $auth", body).execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    val message = when (response.code()) {
                        401 -> FAILED_LOGOUT
                        422 -> ""
                        else -> ERROR_MESSAGE
                    }

                    Pair(response.code(), message)
                }
            }

        } catch (e: Exception) {
            showLog("failed add point : ${e.message}")
            Pair(ResponseCode.TIME_OUT, FAILED_MESSAGE)
        }
    }

    override fun getPoint(auth: String): Pair<Int, String?> {
        return try {
            val response = api.getPoint("Bearer $auth").execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {
                    val message = when (response.code()) {
                        401 -> FAILED_LOGOUT
                        else -> ERROR_MESSAGE
                    }

                    Pair(response.code(), message)
                }
            }

        } catch (e: Exception) {
            showLog("failed get iklan : ${e.message}")
            Pair(ResponseCode.TIME_OUT, FAILED_MESSAGE)
        }
    }

}
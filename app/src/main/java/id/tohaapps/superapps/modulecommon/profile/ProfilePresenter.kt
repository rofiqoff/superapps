package id.tohaapps.superapps.modulecommon.profile

import android.content.Context
import id.tohaapps.superapps.repository.AppApiClient
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.repository.data.AppSession
import id.tohaapps.superapps.support.stringToObject
import id.tohaapps.superapps.view.login.model.User
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject

class ProfilePresenter(var context: Context, var view: ViewNetworkState) : IProfilePresenter {
    private val iteractor by lazy { ProfileIteractor(AppApiClient.mainClient()) }
    private val session by lazy { AppSession(context) }

    val profileParam = "profile.param"

    override fun getProfile() {
        view.networkState = NetworkingState.ShowLoading(Pair(profileParam, true))
        GlobalScope.launch {
            val auth = session.profile?.token

            val response = iteractor.getProfile(auth.toString())

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(profileParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        val body = JSONObject(response.second.toString())

                        val dataBody = body.getString("data")
                        val data = stringToObject(dataBody, User::class.java)

                        session.updateSession(data)

                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(profileParam, "success"))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            profileParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }
}
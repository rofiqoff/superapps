package id.tohaapps.superapps.modulecommon.profile

import id.tohaapps.superapps.repository.api.MainApi
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.support.ERROR_MESSAGE
import id.tohaapps.superapps.support.FAILED_LOGOUT
import id.tohaapps.superapps.support.FAILED_MESSAGE
import id.tohaapps.superapps.support.showLog

class ProfileIteractor(var api: MainApi) : IProfileIteractor {
    override fun getProfile(auth: String): Pair<Int, String?> {
        return try {
            val response = api.getProfile("Bearer $auth").execute()
            when (response.isSuccessful) {
                true -> Pair(ResponseCode.OK, response.body())
                else -> {

                    val message = when (response.code()) {
                        401 -> FAILED_LOGOUT
                        else -> ERROR_MESSAGE
                    }

                    Pair(response.code(), message)
                }
            }

        } catch (e: Exception) {
            showLog("failed get profile : ${e.message}")
            Pair(ResponseCode.TIME_OUT, FAILED_MESSAGE)
        }
    }
}
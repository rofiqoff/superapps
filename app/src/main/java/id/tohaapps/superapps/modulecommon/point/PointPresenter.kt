package id.tohaapps.superapps.modulecommon.point

import android.content.Context
import id.tohaapps.superapps.repository.AppApiClient
import id.tohaapps.superapps.repository.NetworkingState
import id.tohaapps.superapps.repository.ViewNetworkState
import id.tohaapps.superapps.repository.api.ResponseCode
import id.tohaapps.superapps.repository.data.AppSession
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.FormBody
import org.json.JSONObject

class PointPresenter(var context: Context, var view: ViewNetworkState) : IPointPresenter {

    private val iteractor by lazy { PointIteractor(AppApiClient.mainClient()) }

    val getPointParam = "get.point.param"
    val addPointParam = "add.point.param"
    val addPointShareParam = "add.point.share.param"
    val addPointWatchParam = "add.point.watch.param"

    override fun addPoint(iklanId: String, memo: String) {
        view.networkState = NetworkingState.ShowLoading(Pair(addPointParam, true))

        val body = FormBody.Builder().apply {
            add("iklan_id", iklanId)
            add("memo", "Iklan $memo")
        }.build()

        GlobalScope.launch {
            val response = iteractor.addPoint(AppSession(context).profile?.token.toString(), body)

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(addPointParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(addPointParam, response.second))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            addPointParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }

    override fun addPointShare(iklanId: String, memo: String) {
        view.networkState = NetworkingState.ShowLoading(Pair(addPointShareParam, true))

        val body = FormBody.Builder().apply {
            add("iklan_id", iklanId)
            add("memo", "Share $memo")
        }.build()

        GlobalScope.launch {
            val response =
                iteractor.addPointShare(AppSession(context).profile?.token.toString(), body)

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(addPointShareParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        val json = JSONObject(response.second.toString())
                        val data = json.getString("data")
                        val jsonData = JSONObject(data)
                        val reward = jsonData.getString("reward")

                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(addPointShareParam, reward))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            addPointShareParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }

    override fun addPointWatch(iklanId: String, memo: String) {
        view.networkState = NetworkingState.ShowLoading(Pair(addPointWatchParam, true))

        val body = FormBody.Builder().apply {
            add("iklan_id", iklanId)
            add("memo", "Watch $memo")
        }.build()

        GlobalScope.launch {
            val response =
                iteractor.addPointWatch(AppSession(context).profile?.token.toString(), body)

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(addPointWatchParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        val json = JSONObject(response.second.toString())
                        val data = json.getString("data")
                        val jsonData = JSONObject(data)
                        val reward = jsonData.getString("reward")

                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(addPointWatchParam, reward))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            addPointWatchParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }

    override fun getPoint() {
        view.networkState = NetworkingState.ShowLoading(Pair(getPointParam, true))
        GlobalScope.launch {
            val response = iteractor.getPoint(AppSession(context).profile?.token.toString())

            (view.networkState !is NetworkingState.Destroy).apply {
                view.networkState = NetworkingState.ShowLoading(Pair(getPointParam, false))
                when (response.first) {
                    ResponseCode.OK -> {
                        view.networkState =
                            NetworkingState.ResponseSuccess(Pair(getPointParam, response.second))
                    }
                    else -> view.networkState = NetworkingState.ResponseFailure(
                        Pair(
                            getPointParam,
                            Pair(response.first, response.second)
                        )
                    )
                }

            }
        }
    }
}
package id.tohaapps.superapps.modulecommon.point

import okhttp3.FormBody

interface IPointIteractor {
    fun addPoint(auth: String, body: FormBody): Pair<Int, String?>
    fun addPointShare(auth: String, body: FormBody): Pair<Int, String?>
    fun addPointWatch(auth: String, body: FormBody): Pair<Int, String?>
    fun getPoint(auth: String): Pair<Int, String?>
}

interface IPointPresenter {
    fun getPoint()
    fun addPoint(iklanId: String, memo: String)
    fun addPointShare(iklanId: String, memo: String)
    fun addPointWatch(iklanId: String, memo: String)
}
package id.tohaapps.superapps.modulecommon.profile

interface IProfileIteractor {
    fun getProfile(auth: String): Pair<Int, String?>
}

interface IProfilePresenter {
    fun getProfile()
}